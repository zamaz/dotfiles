#!/bin/bash
#
# Author: Amazigh Zerzour
# Date: 19 Dec 2019
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
SYSTEMD_FOLDER="$HOME/.config/systemd/user/"

[ ! -d "$SYSTEMD_FOLDER" ] && mkdir -p "$SYSTEMD_FOLDER"

sudo cp -v "$DIR/mbsync.service" "$SYSTEMD_FOLDER"
sudo cp -v "$DIR/mbsync.timer" "$SYSTEMD_FOLDER"

systemctl --user daemon-reload &&
	systemctl --user start mbsync.timer &&
	systemctl --user enable mbsync.timer
