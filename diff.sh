#! /bin/bash
# Get aliases of files that have changed
SILENT="$1"
function say() {
    [[ "$SILENT" != "silent" ]] && echo "$1"
}

say "Getting list of changed files..."
IGNORE=(f_main)
FILES_ALIASES="$(dotdrop compare -i ~/.vimrc 2>/dev/null | awk -F: '/compare / {print $1}' | cut -d' ' -f3)"

# Ignore empty result set
if [[ -z "$FILES_ALIASES" ]]; then
    say "No changed files found"
	exit
fi

# Ignore ignore files
for ignore in "${IGNORE[@]}"; do
    say "Ignoring $ignore"
    FILES_ALIASES="$(echo "$FILES_ALIASES" | sed '/'"$ignore"'/d')"
done

while read -r FILE_ALIAS <&3; do
	# For each changed file alias, get the dotdrop file and the real system file
	FNAMES="$(dotdrop files -bG 2>/dev/null |
		grep "$FILE_ALIAS" |
        sed 's/^.*dst:\([^,]*\).*src:\([^,]*\).*$/\1 \2/')"
    FILE_REAL="$(echo "$FNAMES" | awk '{print $1}')"
	FILE_DOTDROP="$(echo "$FNAMES" | awk '{print $2}')"
	# Expand the tilde symbol. Otherwise, FILE_REAL='~/.<dotfile>'
	FILE_REAL="${FILE_REAL/#\~/$HOME}"
    FILE_DOTDROP="${FILE_DOTDROP/#\~/$HOME}"

	# Ignore errors getting filenames
	if [[ ! -f "$FILE_DOTDROP" ]] || [[ ! -f "$FILE_REAL" ]]; then
		echo "Error getting file names for $FILE_ALIAS"
		continue
	fi

	# Ignore empty files. Empty files occur when they exist in dotdrop but have
	# not been deployed locally yet
	if [[ ! -s "$FILE_DOTDROP" ]] || [[ ! -s "$FILE_REAL" ]]; then
		echo "One or more of the files are empty. Skipping $FILE_ALIAS"
		echo "$FILE_DOTDROP: $(stat --printf="%s" "$FILE_DOTDROP")"
		echo "$FILE_REAL: $(stat --printf="%s" "$FILE_REAL")"
		echo
		continue
	fi

	# Diff the files and map q to qa to close vim quickly
	read -p "Diff $FILE_DOTDROP against $FILE_REAL? [Y/n]: " -n 1 -r DIFF
	echo

	if [[ $DIFF =~ ^[Yy]$ ]] || [ -z "$DIFF" ]; then
		# Parse any jinja directives
		FILE_DOTDROP_PARSED="/tmp/$(basename "$FILE_DOTDROP")"

        say "Parsing $FILE_DOTDROP as $FILE_DOTDROP_PARSED..."
		if ! ./parse-directives.py "$FILE_DOTDROP" "$FILE_DOTDROP_PARSED"; then
			echo "ERROR: Failed to parse file $FILE_DOTDROP"
            echo "Using unparsed file for diff..."
			FILE_DOTDROP_PARSED="$FILE_DOTDROP"
		fi

		echo
		vimdiff "$FILE_DOTDROP" "$FILE_DOTDROP_PARSED" "$FILE_REAL" \
			-v \
			-c "command CloseAll :qa" -c "cabbrev q CloseAll" \
			-c "command WriteCloseAll :wqa" -c "cabbrev wq WriteCloseAll" \
			-c "command DPA :diffput 1 | diffput 2 | diffput 3 | diffu" \
			-c "cabbrev diffput DPA" \
			-c "wincmd l | wincmd l" \
			-n
	fi
done 3<<<"$FILES_ALIASES"

git status -s
# The redirect needs to go to a file descriptor different from STDIN for vim to work properly
# See https://unix.stackexchange.com/questions/286263/execute-vim-in-a-loop
