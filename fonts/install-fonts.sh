#!/bin/bash
echo "Installing fonts..."
DIR="$(dirname "$(readlink -f "$0")")"
mkdir -p $HOME/.local/share/fonts
cp -vf $DIR/*.{otf,ttf} $HOME/.local/share/fonts
fc-cache -v | grep -v "skipping"
echo "Done installing fonts"
