#!/bin/bash

# Install oh-my-zsh
echo "### Installing oh-my-zsh"
OHMY_INSTALLED=0
if [ -x "$(command -v curl)" ]; then
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
	OHMY_INSTALLED=1
elif [ -x "$(command -v wget)" ]; then
	sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
	OHMY_INSTALLED=1
else
	echo "Failed to install oh-my-zsh. Install curl or wget first."
	OHMY_INSTALLED=0
fi

# Install oh-my-zsh plugins
echo
echo "### Installing oh-my-zsh plugins"
if [[ $OHMY_INSTALLED -eq 1 ]]; then
	if [ ! -d "$HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions" ]; then
		git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
	else
		echo "Autosuggestions is already installed"
	fi
	if [ ! -d "$HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting" ]; then
		git clone https://github.com/zsh-users/zsh-syntax-highlighting ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
	else
		echo "Syntax highlighting is already installed"
	fi
fi

# Back up dotfiles
echo
echo "### Backing up dotfiles"
mv -fv ~/.vimrc ~/.vimrc.bkp 2>/dev/null
mv -fv ~/.zshrc ~/.zshrc.bkp 2>/dev/null

# Install dotfiles
echo
echo "### Installing dotfiles"
cp -v ./vimrc ~/.vimrc
cp -v ./zshrc ~/.zshrc

# Install vim-related files and folders
echo
echo "### Installing vim addons"
mkdir -pv ~/.vim/{undodir,swapfiles,colors}
cp -v ./vim/colors/monokai.vim ~/.vim/colors/
