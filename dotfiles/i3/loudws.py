#!/usr/bin/env python3
# loudws.py
#
# Print workspace numbers that harbor sound producing programs
#
# Known issues:
#   - Does not work with mpd clients since it's mpd using the sink but only the
#     client has a window. the two processes cannot be linked afaik.
#     WORKAROUND: If mpd is detected, mark all workspaces with clients open.
#     Make sure you have your client configured so that if it changes the
#     window title it starts with its name (or mpd). E.g. for ncmpcpp:
#     song_window_title_format = "ncmpcpp {a} - {t}"
#   - Since the window and workspace are found by comparing the window names to
#     the binary name this script cannot distinguish between the actual program
#     and, say, a Firefox window with a tab focused that starts with the same
#     name. Both workspaces will be marked in such a case.
#   - Since only the start of the window name is matched against the binary
#     name, Firefox is not detected (it appends "Mozilla Firefox").

import sys
import subprocess
from shlex import split
import i3ipc

VERBOSE = len(sys.argv) > 1 and sys.argv[1] in ('v', '-v',
                                                'verbose', '--verbose')
MPD_CLIENTS = ['ncmpcpp', 'mopidy']
MATCH_CONDITION = 'contains'


def get_full_path(comm):
    cmd = 'which {}'.format(comm)
    which = subprocess.Popen(split(cmd), stdout=subprocess.PIPE)
    out, err = which.communicate()

    if err:
        verbose_print(err.decode('utf-8'))
        return None

    commpath = out.decode('utf-8').strip().strip('"') or None
    verbose_print("Full path of {}: {}".format(comm, commpath))
    return commpath


def verbose_print(txt):
    if VERBOSE:
        print(txt)

class Process:
    def __init__(self, pid=None, binary=None):
        self.binary = binary
        self.pid = pid
        self.parent = self._get_parent()
        self.parentpath = get_full_path(self.parent)
        self.path = get_full_path(binary)

    def _get_parent(self):
        cmd = "ps -hp {} -o ppid".format(self.pid)
        ps1 = subprocess.Popen(split(cmd), stdout=subprocess.PIPE)
        cmd = "xargs -i ps -hp {} -o comm"
        ps2 = subprocess.Popen(split(cmd), stdin=ps1.stdout,
                               stdout=subprocess.PIPE)
        out, err = ps2.communicate()

        if err:
            print(err.decode('utf-8'))
            return None

        return out.decode('utf-8').strip()


def get_loud_processes():
    procs = []
    # Get names and pids of sound producing processes
    cmd = "pacmd list-sink-inputs"
    pacmd = subprocess.Popen(split(cmd), stdout=subprocess.PIPE)
    cmd = "awk -F= '/application.process.(binary|id)/ {print $2}'"
    awk = subprocess.Popen(
        split(cmd), stdin=pacmd.stdout, stdout=subprocess.PIPE)
    out, err = awk.communicate()

    if err:
        verbose_print(err)
        return procs

    # Split lines into array and remove any quotation marks
    out_lines = [el.strip().replace('"', '')
                 for el in out.decode('utf-8').split('\n') if el]

    # Every two lines belong to one process
    for i in range(0, len(out_lines), 2):
        pid = out_lines[i]
        binary = out_lines[i+1]
        procs.append(Process(pid=pid, binary=binary))

    # If mpd is playing match any mpd clients
    proc_names = [p.binary for p in procs]
    if 'mpd' in proc_names or get_full_path('mpd') in proc_names:
        for mpd_client in MPD_CLIENTS:
            procs.append(Process(binary=mpd_client))

    return procs

def get_all_windows():
    # Get names of all windows from i3 tree
    # Confusingly, windows can have both 'workspace' and 'con' as type and
    # workspaces have 'con' as type.
    i3 = i3ipc.Connection()
    windows = [el for el in i3.get_tree() if el.type in ('con', 'workspace')]
    nodes = [n for w in windows for n in w.nodes]
    window_names = [n.name for n in nodes]
    verbose_print("Window names: ")
    verbose_print(window_names)
    verbose_print('')
    return nodes

def match(haystack, needle):
    if not needle:
        return False
    verbose_print("Matching {} against {}".format(haystack, needle))
    if MATCH_CONDITION == 'startswith':
        return haystack.lower().startswith(needle)
    elif MATCH_CONDITION == 'contains':
        return needle in haystack.lower()
    else:
        print("Unknown match condition")
        return False

def procs2ws(procs):
    nodes = get_all_windows()
    ws_nums = [node.workspace().num for proc in procs for node in nodes
               if node.name
               and (
                   match(node.name, proc.binary)
                   or match(node.name, proc.path)
                   or match(node.name, proc.parent)
                   or match(node.name, proc.parentpath)
               )]
    return list(set(ws_nums))

def get_loud_workspaces():
    workspaces = []
    procs = get_loud_processes()

    if len(procs) == 0:
        verbose_print("No sound producing programs detected")
        return workspaces

    verbose_print('\nPrograms producing sound: ' + str(len(procs)))
    for proc in procs:
        verbose_print("{:6s} {} ({})".format(proc.pid, proc.binary,
                                             proc.parent))
    verbose_print('')

    workspaces = procs2ws(procs)

    if len(workspaces) == 0:
        verbose_print(
            "None of the sound producing programs belong to any windows")
        return workspaces

    verbose_print("Workspaces hosting loud windows: ")
    verbose_print(workspaces)

    return workspaces


if __name__ == '__main__':
    get_loud_workspaces()
