#! /usr/bin/env python3
import subprocess
import i3ipc


def snatch_ws_number(ws1, ws2):
    """
    Replace number of ws1 in its name with the number of ws2

    Arguments:
        ws1 Workspace to rename
        ws2 Workspace whose number to assign to ws1 or its name

    Return:
        Name of ws1 with its workspace number replaced

    Example:
        ws1 = {'num': 1, 'name':'1: workspace 1', ...}
        ws2 = {'num': 2, 'name':'2: workspace 2', ...}

        Return value = '2: workspace 1'
    """
    try:
        ws2num = ws2.num
    except AttributeError:
        ws2num = ws2.split(':')[0]
    ws1_name_new = ws1.name.replace(str(ws1.num), str(ws2num))
    return ws1_name_new


def rename_workspace(old_name, new_name):
    cmd = r'i3-msg "rename workspace \"{}\" to \"{}\""'.format(old_name,
                                                               new_name)
    print(cmd)
    subprocess.call(cmd, shell=True)


def sort_ws_num(ws_arr):
    return sorted(ws_arr, key=lambda x: int(x.split(':')[0]))


i3 = i3ipc.Connection()
workspaces = i3.get_workspaces()
workspace_names = [ws.name for ws in workspaces]
workspace_numbers = [ws.num for ws in workspaces]
dummy_name = 'the upside down'

# Get current workspace
ws_focused = [ws for ws in workspaces if ws.focused][0]

# Get names of workspaces to echo to rofi
visible_workspaces = [wsn for wsn in workspace_names
                      if wsn != ws_focused.name]
missing = ['{num}:{num}'.format(num=i) for i in range(1, 11)
           if i not in workspace_numbers]
names = sort_ws_num(visible_workspaces + missing)
names = '\n'.join(names)
echo = "{}\n".format(names).encode('utf-8')

# Call rofi to ask which workspace to switch with
cmd = 'rofi -dmenu -p "Which workspace to switch with?"'
rofi_proc = subprocess.Popen(cmd, stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE, shell=True)
out, err = rofi_proc.communicate(echo)

if err:
    exit(err)
if not out:
    exit("No workspace selected")

switch_name = out.decode('utf-8').strip()

# Get workspace to switch to
ws_switch = next((ws for ws in workspaces if ws.name == switch_name), None)

# If the chosen workspace does not exist, don't worry about renaming it too
if not ws_switch:
    ws_focused_new = snatch_ws_number(ws_focused, switch_name)
    rename_workspace(ws_focused.name, ws_focused_new)
    exit()

# Switch workspace numbers
ws_focused_new = snatch_ws_number(ws_focused, ws_switch)
ws_switch_new = snatch_ws_number(ws_switch, ws_focused)

# Rename workspaces
rename_workspace(ws_focused.name, dummy_name)
rename_workspace(ws_switch.name, ws_switch_new)
rename_workspace(dummy_name, ws_focused_new)
