#!/usr/bin/env python3
# Credit: u/RyanDwyer
# https://www.reddit.com/r/i3wm/comments/8njtwg/anyone_need_to_quickly_switch_to_the_nearest/

import json
import subprocess

output = subprocess.check_output(['i3-msg', '-t', 'get_workspaces'])
workspaces = json.loads(output)

next_num = next((i for i in range(1, 11) if not [ws for ws in workspaces if
                                                ws['num'] == i]), None)

if not next_num:
    subprocess.call(['notify-send', 'i3', 'No empty workspaces left'])
else:
    print("Next empty workspace: WS" + str(next_num))
    subprocess.call(['i3-msg', 'workspace number %i' % next_num])
