# Amazigh Zerzour
# Apr 11 2019
#
# Toggle floating of currently focused window setting a fixed size when
# floating and restoring the original size when tiling back
import i3ipc
import json

i3 = i3ipc.Connection()
tree = i3.get_tree()
focused = tree.find_focused()

winSizeFile = '/tmp/winSizes.json'
DEFAULT_SIZE = ('700 px', '500 px')
FLOAT_SIZE = ('50 ppt', '50 ppt')

try:
    with open(winSizeFile, 'r') as f:
        prevSizes = json.load(f)
except IOError:
    prevSizes = {}

def isFloating(win):
    return win.floating in ('auto_on', 'user_on')

def getPrevSize(win):
    return prevSizes[win.id] if win.id in prevSizes.keys() else DEFAULT_SIZE

def saveCurrentSize(win):
    h = win.window_rect.height
    w = win.window_rect.width
    prevSizes.update({win.id: (h, w)})
    with open(winSizeFile, 'w') as f:
        json.dump(prevSizes, f)

if isFloating(focused):
    w, h = getPrevSize(focused)
    focused.command('resize set {} {}'.format(w, h))
    focused.command('floating disable')
else:
    saveCurrentSize(focused)
    focused.command('floating enable')
    focused.command('resize set {} {}'.format(*FLOAT_SIZE))
    focused.command('move position center')
