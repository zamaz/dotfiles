#!/usr/bin/env python3
import subprocess
import lockfile
import os
import time

import i3ipc
from shlex import split

from autorename_workspaces import rename_workspaces

lockfpath = '/var/run/loudws.pid'

if os.path.exists(lockfpath):
    exit('Another instance of this daemon is already running')

i3 = i3ipc.Connection()

def check_for_sound_changes(i3, oldoutput):
    cmd = "pacmd list-sink-inputs"
    pacmd = subprocess.Popen(split(cmd), stdout=subprocess.PIPE)
    cmd = "awk -F= '/application.process.id/ {print $2}'"
    awk = subprocess.Popen(split(cmd), stdin=pacmd.stdout, stdout=subprocess.PIPE)
    output, err = awk.communicate()

    output = output.decode('utf-8').strip().strip('"')

    if err:
        print(err)
        return ''

    if output != oldoutput:
        print("Output changed")
        rename_workspaces(i3)

    return output

if __name__ == '__main__':
    print("Starting daemon...")
    oldoutput = ''
    while True:
        oldoutput = check_for_sound_changes(i3, oldoutput)
        time.sleep(.5)
