#!/usr/bin/env python3
#
# github.com/justbuchanan/i3scripts
#
# This script listens for i3 events and updates workspace names to show icons
# for running programs.  It contains icons for a few programs, but more can
# easily be added by editing the WINDOW_ICONS list below.
#
# It also re-numbers workspaces in ascending order with one skipped number
# between monitors (leaving a gap for a new workspace to be created). By
# default, i3 workspace numbers are sticky, so they quickly get out of order.
#
# Dependencies
# * xorg-xprop  - install through system package manager
# * i3ipc       - install with pip
# * fontawesome - install with pip
#
# Installation:
# * Download this repo and place it in ~/.config/i3/ (or anywhere you want)
# * Add "exec_always ~/.config/i3/i3scripts/autoname_workspaces.py &" to your i3 config
# * Restart i3: $ i3-msg restart
#
# Configuration:
# The default i3 config's keybindings reference workspaces by name, which is an
# issue when using this script because the "names" are constantly changing to
# include window icons.  Instead, you'll need to change the keybindings to
# reference workspaces by number.  Change lines like:
#   bindsym $mod+1 workspace 1
# To:
#   bindsym $mod+1 workspace number 1

import i3ipc
import logging
import signal
import sys
import fontawesome as fa

from util import *
#import loudws

# Add icons here for common programs you use.  The keys are the X window class
# (WM_CLASS) names (lower-cased) and the icons can be any text you want to
# display.
#
# Most of these are character codes for font awesome:
#   http://fortawesome.github.io/Font-Awesome/icons/
#
# If you're not sure what the WM_CLASS is for your application, you can use
# xprop (https://linux.die.net/man/1/xprop). Run `xprop | grep WM_CLASS`
# then click on the application you want to inspect.
WINDOW_ICONS = {
    'alacritty': fa.icons['terminal'],
    'arandr': fa.icons['tv'],
    'atom': fa.icons['code'],
    'apache directory studio': fa.icons['tree'],
    'banshee': fa.icons['play'],
    'baobab': fa.icons['database'],
    'brave-browser': fa.icons['chrome'],
    'birdfont': fa.icons['font'],
    'blender': fa.icons['cube'],
    'blueman-manager': fa.icons['bluetooth'],
    'blueman-sendto': fa.icons['bluetooth'],
    'chromium': fa.icons['chrome'],
    'calibre': fa.icons['book'],
    'cava': fa.icons['chart-bar'],
    'cura': fa.icons['cube'],
    'darktable': fa.icons['image'],
    'dbeaver': fa.icons['database'],
    'discord': fa.icons['comment'],
    'dolphin': fa.icons['folder'],
    'eclipse': fa.icons['code'],
    'emacs': fa.icons['code'],
    'eog': fa.icons['image'],
    'evince': fa.icons['file-pdf'],
    'evolution': fa.icons['envelope'],
    'feh': fa.icons['image'],
    'file-roller': fa.icons['compress'],
    'firefox': fa.icons['firefox'],
    'firefox-esr': fa.icons['firefox'],
    'fontforge': fa.icons['font'],
    'gimp': fa.icons['image'],
    'gnome-control-center': fa.icons['toggle-on'],
    'gnome-terminal-server': fa.icons['terminal'],
    'google-chrome': fa.icons['chrome'],
    'gpick': fa.icons['eye-dropper'],
    'imv': fa.icons['image'],
    'inkscape': fa.icons['paint-brush'],
    'java': fa.icons['code'],
    'jetbrains-studio': fa.icons['code'],
    'keybase': fa.icons['key'],
    'kicad': fa.icons['microchip'],
    'kitty': fa.icons['terminal'],
    'kmag': fa.icons['search-plus'],
    'konsole': fa.icons['terminal'],
    'keepass2': fa.icons['key'],
    'keepassxc': fa.icons['key'],
    'libreoffice': fa.icons['file-alt'],
    'lua5.1': fa.icons['moon'],
    'mlterm': fa.icons['terminal'],
    'mail': fa.icons['envelope'],
    'mattermost': fa.icons['envelope'],
    'mutt': fa.icons['envelope'],
    'mpv': fa.icons['tv'],
    'mupdf': fa.icons['file-pdf'],
    'mysql-workbench-bin': fa.icons['database'],
    'ncmpcpp': fa.icons['headphones'],
    'nautilus': fa.icons['copy'],
    'nemo': fa.icons['copy'],
    'nm-applet': fa.icons['wifi'],
    'openscad': fa.icons['cube'],
    'okular': fa.icons['file-pdf'],
    'owncloud': fa.icons['cloud'],
    'pavucontrol-qt': fa.icons['volume-up'],
    'pencil': fa.icons['paint-brush'],
    'postman': fa.icons['space-shuttle'],
    'remmina': fa.icons['desktop'],
    'reddit': fa.icons['reddit'],
    'rhythmbox': fa.icons['play'],
    'ranger': fa.icons['folder'],
    'signal': fa.icons['envelope-open'],
    'slack': fa.icons['slack'],
    'slic3r.pl': fa.icons['cube'],
    'spotify': fa.icons['spotify'],
    'steam': fa.icons['steam'],
    'st-256color': fa.icons['terminal'],
    'st': fa.icons['terminal'],
    'subl': fa.icons['file-alt'],
    'subl3': fa.icons['file-alt'],
    'sublime_text': fa.icons['file-alt'],
    'surf': fa.icons['globe'],
    'thunar': fa.icons['copy'],
    'thunderbird': fa.icons['envelope'],
    'totem': fa.icons['play'],
    'qutebrowser': fa.icons['globe'],
    'urxvt': fa.icons['terminal'],
    'vim': fa.icons['code'],
    'gvim': fa.icons['code'],
    'vimwiki': fa.icons['wikipedia-w'],
    'virtualbox manager': fa.icons['snapchat-ghost'],
    'virtualbox machine': fa.icons['snapchat-ghost'],
    'vivaldi-stable': fa.icons['chrome'],
    'wireshark': fa.icons['traffic-light'],
    'xfreerdp': fa.icons['desktop'],
    'xterm': fa.icons['terminal'],
    'xfce4-terminal': fa.icons['terminal'],
    'xournal': fa.icons['file-alt'],
    'yelp': fa.icons['code'],
    'zathura': fa.icons['file-pdf'],
    'zenity': fa.icons['window-maximize'],
}

# This icon is used for any application not in the list above
DEFAULT_ICON = ''

def get_icon(window):
    # Try all window classes and use the first one we have an icon for
    names = xprop(window.window, 'WM_NAME')
    if names is not None and len(names) > 0:
        for name in names:
            name = name.lower()  # case-insensitive matching
            for key in WINDOW_ICONS:
                if name.split()[0] == key:
                    return WINDOW_ICONS[key]

    classes = xprop(window.window, 'WM_CLASS')
    if classes != None and len(classes) > 0:
        for cls in classes:
            cls = cls.lower()  # case-insensitive matching
            if cls in WINDOW_ICONS:
                return WINDOW_ICONS[cls]
    logging.info(
        'No icon available for window with classes: %s' % str(classes))
    return DEFAULT_ICON


# renames all workspaces based on the windows present
# also renumbers them in ascending order, with one gap left between monitors
# for example: workspace numbering on two monitors: [1, 2, 3], [5, 6]
def rename_workspaces(i3):
    #loud_workspaces = loudws.get_loud_workspaces()
    for ws_index, workspace in enumerate(i3.get_tree().workspaces()):
        # Ignore sticky windows to not mess up 'back_and_forth'. Sticky windows
        # are always visible anyway so no need to show them in the bar.
        icons = [get_icon(w) for w in workspace.leaves() if not w.sticky]

        # Cast icons into a set so that each icon only shows once. Also sort
        # them so they always appear in the same order.
        new_icons = ' '.join(sorted(set(icons)))

        name_parts = parse_workspace_name(workspace.name)
        prefix = name_parts.num
        #prefix = '◉ ' + prefix if ws_index in loud_workspaces else prefix

        new_name = construct_workspace_name(
            NameParts(
                num=name_parts.num, prefix=prefix, shortname=name_parts.shortname,
                icons=new_icons))

        if workspace.name == new_name:
            continue
        print('rename workspace "%s" to "%s"' % (workspace.name, new_name))
        i3.command(
            'rename workspace "%s" to "%s"' % (workspace.name, new_name))


# Rename workspaces to just numbers and shortnames, removing the icons.
def on_exit(i3):
    for workspace in i3.get_tree().workspaces():
        name_parts = parse_workspace_name(workspace.name)
        new_name = construct_workspace_name(
            NameParts(
                num=name_parts.num, shortname=name_parts.shortname,
                icons=None))
        if workspace.name == new_name:
            continue
        i3.command(
            'rename workspace "%s" to "%s"' % (workspace.name, new_name))
    i3.main_quit()
    sys.exit(0)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    i3 = i3ipc.Connection()

    # Exit gracefully when ctrl+c is pressed
    for sig in [signal.SIGINT, signal.SIGTERM]:
        signal.signal(sig, lambda signal, frame: on_exit(i3))

    rename_workspaces(i3)

    # Call rename_workspaces() for relevant window events
    def event_handler(i3, e):
        if e.change in ['new', 'close', 'move']:
            rename_workspaces(i3)

    i3.on('window', event_handler)
    i3.on('workspace::move', event_handler)
    i3.main()
