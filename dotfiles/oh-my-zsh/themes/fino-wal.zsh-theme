# fino.zsh-theme

# Use with a dark background and 256-color terminal!
# Meant for people with rbenv and git. Tested only on OS X 10.7.

# You can set your computer name in the ~/.box-name file if you want.

# Borrowing shamelessly from these oh-my-zsh themes:
#   bira
#   robbyrussell
#
# Also borrowing from http://stevelosh.com/blog/2010/02/my-extravagant-zsh-prompt/

function prompt_char {
  [ "$(id -u)" -eq 0 ] && echo "#" && return
	echo "$"
}

function box_name {
    [ -f ~/.box-name ] && cat ~/.box-name || echo ${SHORT_HOST:-$HOST}
}

local ruby_env=''
if which rvm-prompt &> /dev/null; then
  ruby_env='using%{$FG[243]%} ‹$(rvm-prompt i v g)›%{$reset_color%}'
else
  if which rbenv &> /dev/null; then
    ruby_env='using%{$FG[243]%} ‹$(rbenv version-name)›%{$reset_color%}'
  fi
fi

# http://unix.stackexchange.com/a/269085/67282
function fromhex() {
  hex=$1
  if [[ $hex == "#"* ]]; then
    hex=$(echo $1 | awk '{print substr($0,2)}')
  fi
  r=$(printf '0x%0.2s' "$hex")
  g=$(printf '0x%0.2s' ${hex#??})
  b=$(printf '0x%0.2s' ${hex#????})
  echo -e `printf "%03d" "$(((r<75?0:(r-35)/40)*6*6+(g<75?0:(g-35)/40)*6+(b<75?0:(b-35)/40)+16))"`
}

source ~/.cache/wal/colors.sh

ASCII_COLOR1="$(fromhex "$color1")"
ASCII_COLOR2="$(fromhex "$color2")"
ASCII_COLOR3="$(fromhex "$color3")"

local git_info='$(git_prompt_info)'
local prompt_char='$(prompt_char)'

PROMPT="%B%{$FG[$ASCII_COLOR1]%}%n%{$reset_color%} %B%{$FG[239]%}at%{$reset_color%} %B%{$FG[$ASCII_COLOR2]%}$(box_name)%{$reset_color%} %B%{$FG[239]%}in%{$reset_color%} %B%{$terminfo[bold]$FG[$ASCII_COLOR3]%}%~%{$reset_color%}${git_info} %{$FG[239]%}${ruby_env}
${prompt_char}%{$reset_color%} "

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$FG[239]%}on%{$reset_color%} %{$fg[255]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$FG[202]%} ✘"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$FG[040]%} ✔"
