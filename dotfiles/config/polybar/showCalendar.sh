#!/bin/sh
pkill -f "plasmawindowed org.kde.plasma.calendar"

while $(pgrep -f "plasmawindowed org.kde.plasma.calendar"); do
	sleep 1
done

plasmawindowed org.kde.plasma.calendar&
