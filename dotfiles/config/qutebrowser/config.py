## BASIC {{{1
config.load_autoconfig()
c.url.default_page = 'https://duckduckgo.com'
c.url.start_pages = 'https://duckduckgo.com'
c.url.open_base_url = True
c.url.searchengines = {"DEFAULT": "https://duckduckgo.com/?q={}",
                       "you": "https://youtube.com/search?q={}",
                       "go": "https://google.com/search?q={}",
                       "wiki": "https://en.wikipedia.com/wiki/{}",
                       "so": "https://stackoverflow.com/search?q={}",
                       "red": "https://reddit.com/search?q={}",
                       "eco": "https://ecosia.org/?q={}"
                       }

# Invoke external editor while a text field has focus by pressing Ctrl-e in insert mode
c.editor.command = ['st', '-e', 'vim', '{file}', "-c", "normal {line}G{column0}l"]

# Execute /usr/share/qutebrowser/scripts/dictcli.py install en-US de-DE
c.spellcheck.languages = ['en-US', 'de-DE']

## KEYBINDINGS {{{1
# Toggle UI {{{2
config.bind('xx', 'config-cycle statusbar.show always never ;; config-cycle tabs.show always switching')
config.bind('xt', 'config-cycle tabs.show always switching')
config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('<Ctrl-shift-c>', 'devtools bottom')

# Videos {{{2
config.bind(
    ',vd', 'spawn youtube-dl -o ~/Videos/YouTube/%(title)s.%(ext)s {url}')
config.bind(',vp', 'spawn mpv {url}')
config.bind(',vP', 'hint links spawn mpv {hint-url}')

# Movement {{{2
config.bind('<Ctrl-shift-j>', 'tab-move +')
config.bind('<Ctrl-shift-k>', 'tab-move -')
config.bind('gh', 'home')
config.bind('<Ctrl-j>', 'prompt-item-focus next', mode='prompt')
config.bind('<Ctrl-k>', 'prompt-item-focus prev', mode='prompt')
config.bind('<Ctrl-j>', 'completion-item-focus --history next', mode='command')
config.bind('<Ctrl-k>', 'completion-item-focus --history prev', mode='command')

# Password manager {{{2
config.bind('<Ctrl-l>', 'spawn --userscript qute-lastpass')

# Downloads {{{2
config.bind('<Ctrl-d>', "prompt-yank -s;;spawn uget-gtk --quiet --folder=Downloads '{primary}';;enter-mode normal", mode='prompt')
config.bind('<Ctrl-shift-d>', "prompt-yank -s;;spawn uget-gtk '{primary}';;enter-mode normal", mode='prompt')
config.bind('gd', 'spawn uget-gtk')
config.bind('gD', 'spawn st -e ranger --cmd="set sort=ctime" --cmd="jump_non" --cmd="linemode sizemtime" /home/zama/Downloads')

# Copy the current URL in vim-plug format to quickly add plugins to vim {{{2
# zsh -c "echo {url} | awk -F/ '{ORS=\"\"; print \"Plug '\''\"\$(NF-1)\"/\"\$NF\"'\''\"}' | xclip -i -selection clipboard"
CMD = 'spawn zsh -c "echo {url} | awk -F/ \'' + \
        r'{ORS=\"\"; print \"Plug ' + \
        r"'\''" + r'\"$(NF-1)\"/\"$NF\"' + \
        r"'\''" + r'\"}' + \
        '\'' + \
        ' | xclip -i -selection clipboard";; ' + \
        'message-info "vim-plug line copied to clipboard"'
config.bind('yv', CMD)

# Activate custom global CSS (dark mode) {{{2
config.bind(',n', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/styles/solarized-dark-everywhere.css ""')

# Misc {{{2
config.bind('pP', 'open -t -- {clipboard}')

# }}}

## APPEARANCE {{{1
# Apply pywal colorscheme
config.source('qutewal.py')

# Make tabs and indicators more visually appealing
c.tabs.indicator.width = 6
c.tabs.padding = {"bottom": 5, "left": 5, "right": 5, "top": 5}

# Make the caret in visual mode more visible
c.content.user_stylesheets = ["~/.config/qutebrowser/styles/caret.css"]

# Set higher default zoom for HiDPI screens
c.zoom.default = "125%"

c.window.title_format = "qutebrowser{title_sep}{audio}{current_title}"
c.tabs.title.format = "{audio}{host} | {current_title}"

font = "DejaVu Sans"
font_strong = "Noto Serif"
c.fonts.tabs.selected = font_strong
c.fonts.tabs.unselected = font_strong
c.fonts.statusbar = font_strong
c.fonts.prompts = font
c.fonts.web.family.standard = font
c.fonts.web.family.cursive = font
c.fonts.web.family.fixed = font
c.fonts.web.family.sans_serif = font
c.fonts.web.family.serif = font
c.fonts.completion.category = font_strong
c.fonts.completion.entry = font
c.fonts.downloads = font
c.fonts.hints = font
c.fonts.messages.error = font
c.fonts.messages.info = font
c.fonts.messages.warning = font

## SECURITY {{{1
# Prevent tracking through fingerprinting
c.qt.args = ['disable-reading-from-canvas']

# Block cookie popups - If you never see them you can't accept them and are
# effectively blocking them... hopefully.
c.content.blocking.adblock.lists = ['https://secure.fanboy.co.nz/fanboy-cookiemonster.txt']

## MISC {{{1
# Remove download progress information as soon as it's finished
c.downloads.remove_finished = 0

# Value to send in the `Accept-Language` header
c.content.headers.accept_language = 'en-US,en;q=0.9,de;q=0.8'

# Prevent websites from showing notifications
c.content.notifications = False

# Allow PDF to open in the browser with pdfjs
c.content.pdfjs = True

# Automatically enter insert mode when text input field gains focus
c.input.insert_mode.auto_load = True

# Enable smooth scrolling
c.scrolling.smooth = False

# Load previous tabs when restarting the browser
c.auto_save.session = True

# Use ranger as file selector
c.fileselect.handler = "external"
c.fileselect.single_file.command = ["st", "ranger", "--choosefile={}"]
c.fileselect.multiple_files.command = ["st", "ranger", "--choosefile={}"]

# MODE LINE {{{1
# vim:foldmethod=marker:foldlevel=0
