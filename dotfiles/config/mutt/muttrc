# General {{{1
set folder = "~/mail"
set ts_enabled = yes					# Set window title to ts_status_format
set pager_index_lines = 10
set pager_stop = yes

set editor = "vim"

set timeout = 30 # Auto-update every 30 seconds

# Encryption with GPG
#set pgp_default_key	= '385CD0412151A8CD68A3263C5D641190EFD3BBE7'
#set pgp_sign_as = '385CD0412151A8CD68A3263C5D641190EFD3BBE7'
#set pgp_show_unusable = no 			# Don't show expired keys
#set pgp_use_gpg_agent = yes

# Encryption with S/MIME
set smime_is_default
set smime_timeout = 300                     # Passphrase expiration
#set crypt_autosign = yes                    # Sign all outgoing mail
#set crypt_replyencrypt = yes                # Auto-encrypt if answering an encrypted message
#set crypt_replysign = yes                   # Auto-sign if answering an signed message
#set crypt_replysignencrypted = yes
set crypt_verify_sig = yes
set smime_keys = "~/.smime/keys"
set smime_certificates = "~/.smime/certificates"
set smime_ca_location = "~/.smime/ca-bundle.crt"
set smime_default_key = "ae664bb1.0"
set smime_sign_as = "ae664bb1.0"
set smime_sign_command="openssl smime -sign -md %d -signer %c -inkey %k -passin stdin -in %f -certfile %i -outform DER"
set smime_encrypt_command="openssl smime -encrypt -%a -outform DER -in %f %c"
set smime_decrypt_command="openssl smime -decrypt -passin stdin -inform DER -in %f -inkey %k -recip %c"
# Verify a signature of type multipart/signed
#set smime_verify_command="openssl smime -verify -inform DER -in %s %C -content %f"
# Verify a signature of type application/x-pkcs7-mime
#set smime_verify_opaque_command="\
#openssl smime -verify -inform DER -in %s %C || \
#openssl smime -verify -inform DER -in %s -noverify 2>/dev/null"

# Algorithm to use for encryption.
# valid choices are aes128, aes192, aes256, rc2-40, rc2-64, rc2-128, des, des3
set smime_encrypt_with="aes256"
# Algorithm for the signature message digest.
# Valid choices are md5, sha1, sha224, sha256, sha384, sha512.
set smime_sign_digest_alg="sha256"

# Number | Flags | Date Time | From/To | (# hidden messages if collapsed) | Subject
set index_format = '%4C %{%Y %b %d %H:%M} %-40.35L %?M?(%2M)&    ? %s'

# Default subject format when forwarding a message
set forward_format = 'Fwd: %s'

# Sorting
set sort = threads
set sort_browser = date
set sort_aux = reverse-last-date-received

auto_view image/jpeg
auto_view text/calendar application/ics
# Enable HTML viewing and prefer html
# http://jasonwryan.com/blog/2012/05/12/mutt/
auto_view text/html
alternative_order text/html text/enriched text/plain

# For newer versions of mutt
set new_mail_command="notify-send 'New email in %f' '%n new messages, %u unread.' &"

set uncollapse_new     = yes  # open threads when new mail
set uncollapse_jump    = yes  # jump to unread message when uncollapse

# Hooks {{{1
# Collapse all threads on start
folder-hook . push '<collapse-all>'

# Accounts {{{1
set realname="Amazigh Zerzour"
set reverse_name=yes
set reverse_realname=yes

folder-hook 'mpcdf' 'source ~/.mutt/accounts/mpcdf'
#folder-hook 'gmail' 'source ~/.mutt/accounts/gmail'
#folder-hook 'smirn' 'source ~/.mutt/accounts/smirn'

source ~/.mutt/accounts/mpcdf
#source ~/.mutt/accounts/gmail
#source ~/.mutt/accounts/smirn
set signature = "~/.mutt/signature"

# Aliases {{{1
set alias_file = '~/.mutt/aliases' # Default location for new aliases
set sort_alias = alias
set reverse_alias = yes
source ~/.mutt/aliases

# Key bindings {{{1
# General remappings
bind editor <space> noop
bind attach,index G last-entry
bind attach,index g first-entry
bind pager j next-line
bind pager k previous-line
#bind browser h goto-parent
bind browser l select-entry
bind pager g top
bind pager G bottom
bind index d delete-message
bind index u undelete-message
bind index,browser \cf search
bind index,pager R group-reply

bind index <space> collapse-thread

bind pager \cj next-undeleted
bind pager \ck previous-undeleted

# Prevent Arrow keys from jumping to next message. This also disables mouse
# scrolling since most terminals translate mouse events to Arrow key strokes.
# Some terminals translate mouse events differently natively (e.g. st) and mutt
# has no influence on that. For example, st translates mouse scroll up as
# '<Ctrl>-Y' and down as '<Ctrl-E>'. This causes mutt to open the 'Edit content
# type' dialog when scrolling down. The only way to fix that is to tell st to
# interpret mouse scrolls differently (see mouse patch) or to remap 'Edit
# content type' to a different key combination.
bind index,pager <Down> noop
bind index,pager <Up> noop
bind index,pager <Left> noop
bind index,pager <Right> noop

# Sidebar {{{1
unmailboxes *
mailboxes imaps://zama@mailhost.mpcdf.mpg.de/Inbox \
    imaps://zama@mailhost.mpcdf.mpg.de/Sent \
    imaps://zama@mailhost.mpcdf.mpg.de/Archives \
    imaps://zama@mailhost.mpcdf.mpg.de/Drafts \
    imaps://zama@mailhost.mpcdf.mpg.de/Certificates \
    imaps://zama@mailhost.mpcdf.mpg.de/Material \
    "imaps://zama@mailhost.mpcdf.mpg.de/DFN Warnings" \
    "imaps://zama@mailhost.mpcdf.mpg.de/DFN-Server" \
    imaps://zama@mailhost.mpcdf.mpg.de/openVAS \
    imaps://zama@mailhost.mpcdf.mpg.de/Snippets \
    "imaps://zama@mailhost.mpcdf.mpg.de/Example mails" \
    "imaps://zama@mailhost.mpcdf.mpg.de/Project - IDM" \
    imaps://zama@mailhost.mpcdf.mpg.de/Trash
#mailboxes imaps://amazigh.zerzour@smtp.gmail.com/INBOX
#mailboxes imaps://smirni.sersala@smtp.gmail.com/INBOX

set sidebar_visible = yes
set sidebar_width = 25
set sidebar_short_path = yes
#set sidebar_delim_chars = ":"
set sidebar_folder_indent
set sidebar_indent_string = "  "
set sidebar_next_new_wrap = yes
set sidebar_format = '%B%?F? [%F]?%* %?N?%N/? %?S?%S?'
bind index,pager K sidebar-prev
bind index,pager J sidebar-next
bind index,pager O sidebar-open
set mail_check_stats

# Macros {{{1
# Open current message in vim
macro index,pager \ee "<enter-command>unset wait_key<Enter><pipe-message> cat | vim -c 'set ft=mail' -<Enter>" "Open message in vim"

# Copy current message to clipboard
macro index,pager \ec "<enter-command>unset wait_key<Enter><pipe-message> cat | xclip -i -selection clipboard<Enter>" "Copy message to clipboard"

# Quickly switch From header
macro compose v "<edit-from>^Uidentity\_<tab>" "Select From"

# Macros for switching accounts
macro index <f2> '<sync-mailbox><enter-command>source ~/.mutt/accounts/mpcdf<enter><change-folder>!<enter>'
macro index <f3> '<sync-mailbox><enter-command>source ~/.mutt/accounts/gmail<enter><change-folder>!<enter>'
macro index <f4> '<sync-mailbox><enter-command>source ~/.mutt/accounts/smirn<enter><change-folder>!<enter>'

# Convert iCal attachments to calcurse events
#macro index,pager \ci <pipe-entry>'mutt2calcurse.sh'<enter> 'Add ical events to calcurse'

# Search using notmuch
bind index,pager S noop
macro index Ss \
	"<enter-command>set my_old_pipe_decode=\$pipe_decode my_old_wait_key=\$wait_key nopipe_decode nowait_key<enter>\
	<shell-escape>notmuch-mutt -r --prompt search<enter>\
	<change-folder-readonly>`echo ${XDG_CACHE_HOME:-$HOME/.cache}/notmuch/mutt/results`<enter>\
	<enter-command>set pipe_decode=\$my_old_pipe_decode wait_key=\$my_old_wait_key<enter>" \
	"notmuch: search mail"

macro index Sr \
	"<enter-command>set my_old_pipe_decode=\$pipe_decode my_old_wait_key=\$wait_key nopipe_decode nowait_key<enter>\
	<pipe-message>notmuch-mutt -r thread<enter>\
	<change-folder-readonly>`echo ${XDG_CACHE_HOME:-$HOME/.cache}/notmuch/mutt/results`<enter>\
	<enter-command>set pipe_decode=\$my_old_pipe_decode wait_key=\$my_old_wait_key<enter>" \
	"notmuch: reconstruct thread"

macro index St \
	"<enter-command>set my_old_pipe_decode=\$pipe_decode my_old_wait_key=\$wait_key nopipe_decode nowait_key<enter>\
	<pipe-message>notmuch-mutt tag -- -inbox<enter>\
	<enter-command>set pipe_decode=\$my_old_pipe_decode wait_key=\$my_old_wait_key<enter>" \
	"notmuch: remove message from inbox"

# Open attachment with
macro attach O \
    "<enter-command>unset wait_key<enter>\
    <shell-escape>rm -f /tmp/mutt-attach<enter>\
    <save-entry><kill-line>/tmp/mutt-attach<enter>\
    <shell-escape> /tmp/mutt-attach &^A\"

# Save email to disk
# https://unix.stackexchange.com/questions/60838/saving-email-as-file-in-mutt
#macro index,pager <F5> "<enter-command>unset wait_key<Enter><pipe-message>cat > /home/zama/test.mail<Enter><shell-escape>echo '~/test.mail' | xclip -i -selection clipboard<Enter>" "Save email to disk"

macro index,pager <F5> "<enter-command>unset wait_key<Enter><pipe-message>~/.mutt/save-message.sh ~/email<Enter>" "Seve email to disk"

# Highlighting {{{1
# https://github.com/cyphar/dotfiles/blob/master/.mutt/muttrc-colours

# Highlight certificate CNs
color body brightred default "CN=[^,]*"
# Highlight quotes
color body blue default   "^> .*"
color body green default  "^>( *>){1}.*"
color body blue default   "^>( *>){2}.*"
color body green default  "^>( *>){3}.*"
color body blue default   "^>( *>){4}.*"
color body green default  "^>( *>){5}.*"
color body blue default   "^>( *>){6}.*"
color body green default  "^>( *>){7}.*"
color body blue default   "^>( *>){8}.*"
color body green default  "^>( *>){9}.*"
color body blue default   "^>( *>){10}.*"
# Highlight quotations
color body green default "\s+[\"'][^\"']*[\"']\s+"
# Highlight code
color body green default "(\`.*\`)|(\`\`\`.*)"
# Highlight tickets, hashtags, and hex colors
color body brightgreen default "#[0-9a-zA-Z]+"
# Highlight hex values
color body brightgreen default "0x[0-9a-f]+"
# Highlight unicode
color body brightgreen default "\\u[0-9a-f]+"
color body green default "(\`.*\`)|(\`\`\`.*)"
color body green default "(\`.*\`)|(\`\`\`.*)"
# Highlight mail header
color hdrdefault blue black
color header brightred black "^X-Spam-Flag: YES"
# Highlight phone numbers
color body brightcyan default "\\+?([0-9]+[:space:])?\\(?[0-9]+\\)?([:space:]?\\([0-9]\\)[0-9]*)?([-[:space:]][0-9]+){1,}([:space:]?[/-][:space:]?[0-9]{1,4})?"
# Colouring of URLs and email addresses. Yes, these regular expressions are
# completely insane but they seem to handle most things.
color body           brightcyan        default        "((@(([0-9a-z-]+\\.)*[0-9a-z-]+\\.?|#[0-9]+|\\[[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\]),)*@(([0-9a-z-]+\\.)*[0-9a-z-]+\\.?|#[0-9]+|\\[[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\]):)?[0-9a-z_.+%$-]+@(([0-9a-z-]+\\.)*[0-9a-z-]+\\.?|#[0-9]+|\\[[0-2]?[0-9]?[0-9]\\.[0-2]?[0-9]?[0-9]\\.[0-2]?[0-9]?[0-9]\\.[0-2]?[0-9]?[0-9]\\])"
color body           brightcyan        default        "([a-z][a-z0-9+-]*://(((([a-z0-9_.!~*'();:&=+$,-]|%[0-9a-f][0-9a-f])*@)?((([a-z0-9]([a-z0-9-]*[a-z0-9])?)\\.)*([a-z]([a-z0-9-]*[a-z0-9])?)\\.?|[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)(:[0-9]+)?)|([a-z0-9_.!~*'()$,;:@&=+-]|%[0-9a-f][0-9a-f])+)(/([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*(;([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*)*(/([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*(;([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*)*)*)?(\\?([a-z0-9_.!~*'();/?:@&=+$,-]|%[0-9a-f][0-9a-f])*)?(#([a-z0-9_.!~*'();/?:@&=+$,-]|%[0-9a-f][0-9a-f])*)?|(www|ftp)\\.(([a-z0-9]([a-z0-9-]*[a-z0-9])?)\\.)*([a-z]([a-z0-9-]*[a-z0-9])?)\\.?(:[0-9]+)?(/([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*(;([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*)*(/([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*(;([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*)*)*)?(\\?([-a-z0-9_.!~*'();/?:@&=+$,]|%[0-9a-f][0-9a-f])*)?(#([-a-z0-9_.!~*'();/?:@&=+$,]|%[0-9a-f][0-9a-f])*)?)[^].,:;!)? \t\r\n<>\"]"
## Make attachment headers nicer to read.
#color attach_headers yellow  default        "^.*$"
#color attach_headers brightyellow  default        "(-T|Type:) [a-z]+/[a-z0-9\-]+"
#color attach_headers brightyellow  default        "Size: [0-9\.]+[KMG]?"

## Add some colouring for GPG output
#color attach_headers green           default        "(Begin|End) signature information"
#color attach_headers green           default        "(The following data is signed|End of signed data)"
color body brightgreen     default        "^Good signature from.*"
color body brightred       default        "^Bad signature from.*"
color body brightred       default        "^BAD signature from.*"
color body brightred       default        "Note: This key has expired!"
color body brightmagenta   default        "Problem signature from.*"
color body brightmagenta   default        "WARNING: This key is not certified with a trusted signature!"
color body brightmagenta   default        "         There is no indication that the signature belongs to the owner."
color body brightmagenta   default        "can't handle these multiple signatures"
color body brightmagenta   default        "signature verification suppressed"
color body brightmagenta   default        "invalid node with packet of type"
## Highlight the relevant elements in a diff
color body green         default         "^diff \-.*"
color body green         default         "^index [a-f0-9].*"
color body green         default         "^\-\-\- .*"
color body green         default         "^[\+]{3} .*"
color body cyan          default         "^[\+][^\+]+.*"
color body blue          default         "^\-[^\-]+.*"
color body brightblue    default         "^@@ .*"
# Forwarded messages
color body blue          default         "^[:blank:]*----- Forwarded message.*"
color body blue          default         "^[:blank:]*(Date|From|To|Subject|User-Agent): .*"
# Shell commands
color body green default "^[:blank:]*\$ .*"
# Markup
color body yellow        default             "(^|[[:space:][:punct:]])\\*[^*]+\\*([[:space:][:punct:]]|$)" # *bold*
color body yellow        default            "[:blank:]+-[-[[:alnum:]]+-[:blank:]+"                         # -strike-through-
color body yellow        default            "(^|[[:space:][:punct:]])_[^_]+_([[:space:][:punct:]]|$)"      # _underline_
color body yellow        default            "(^|[[:space:][:punct:]])/[^/]+/([[:space:][:punct:]]|$)"      # /italic/
# File names i. e. '/path/file'
color body green         default           "\\~?\(/[-_.,a-zA-Z0-9{}äöüß]+\)+"
# Path names i. e. '/path/'
color body green         default           "\(/[-_.a-zA-Z0-9]+\)+/"
# Variables  i. e. '$LOGNAME' or '$0'
color body brightgreen   default           "\\$_?[a-zA-Z][-_a-zA-Z]+"
color body brightgreen   default           "\\$[0-9]+[.,0-9]*"
# Border lines
color body cyan          default           "([:blank:]*[-+=#*~_]){6,}"


# Colors {{{1
# If your terminal supports color, you can spice up Mutt by creating your own
# color scheme. To define the color of an object (type of information), you
# must specify both a foreground color and a background color (it is not
# possible to only specify one or the other).
#
# 
# object can be one of:
#   * attachment
#   * body              (match regexp in the body of messages)
#   * bold              (hiliting bold patterns in the body of messages)
#   * error             (error messages printed by Mutt)
#   * header            (match regexp in the message header)
#   * hdrdefault        (default color of the message header in the pager)
#   * index             (match pattern in the message index)
#   * indicator         (arrow or bar used to indicate the current item in a menu)
#   * markers           (the '+' markers at the beginning of wrapped lines in the pager)
#   * message           (informational messages)
#   * normal            (normal (not quoted) text
#   * quoted            (text matching $quote_regexp in the body of a message)
#   * quoted1, quotedN  (higher levels of quoting)
#   * search            (hiliting of words in the pager)
#   * signature
#   * status            (mode lines used to display info about the mailbox or message)
#   * tilde             (the '~' used to pad blank lines in the pager)
#   * tree              (thread tree drawn in the message index and attachment menu)
#   * underline         (hiliting underlined patterns in the body of messages)
#
#
# foreground and background can be one of the following:
#   * white
#   * black
#   * green
#   * magenta
#   * blue
#   * cyan
#   * yellow
#   * red
#   * default
# foreground can optionally be prefixed with the keyword bright to make the
# foreground color boldfaced (e.g., brightred) or colorX{,X,XX}
#
#  $ for i in {0..255} ; do printf "\x1b[38;5;${i}mcolour${i}\n" ; done | column
#
# NOTE: ** The last matching rule wins! **
#
color tree default default # thread arrows
# https://github.com/altercation/mutt-colors-solarized/blob/master/mutt-colors-solarized-dark-16.muttrc
# index
color index         red    					 default         "~D"                        # deleted messages
color index         cyan      			 default         "~v~(!~N)"                  # collapsed thread with no unread
color index         brightblue       default         "~v~(~N)"                   # collapsed thread with some unread
color index         brightblue       default         "~N~v~(~N)"                 # collapsed thread with unread parent
color index         green     			 default         "~R(!~D)"                   # read but not deleted messages
color index         brightblue 			 default         "~U(!~D)"                   # unread but not deleted messages
color index         yellow    			 default         "~P"                        # messages from me
color index         brightyellow     default         "~T"                        # messages from me
#color index         black           default         ~F                         # flagged
#color index         brightred       default         ~=                         # duplicate messages
#color index         brightgreen     default         "~A!~N!~T!~p!~Q!~F!~D!~P"  # the rest
#color index         J_base          default         "~A~N!~T!~p!~Q!~F!~D"      # the rest, new
#color index         red             default         "~A"                        # all messages
#color index         brightred       default         "~E"                        # expired messages
#color index         blue            default         "~N"                        # new messages
#color index         blue            default         "~O"                        # old messages
#color index         brightmagenta   default         "~Q"                        # messages that have been replied to
#color index         blue            default         "~U~$"                      # unread, unreferenced messages
#color index         brightyellow    default         "~v"                        # messages part of a collapsed thread
#color index         cyan            default         "~p!~F"                     # messages to me
#color index         cyan            default         "~N~p!~F"                   # new messages to me
#color index         cyan            default         "~U~p!~F"                   # unread messages to me
#color index         brightgreen     default         "~R~p!~F"                   # messages to me
#color index         red             default         "~F"                        # flagged messages
#color index         red             default         "~F~p"                      # flagged messages to me
#color index         red             default         "~N~F"                      # new flagged messages
#color index         red             default         "~N~F~p"                    # new flagged messages to me
#color index         red             default         "~U~F~p"                    # new flagged messages to me
# statusbg used to indicated flagged when foreground color shows other status
# for collapsed thread
#color index         red             black           "~v~(~F)!~N"                # collapsed thread with flagged, no unread
#color index         yellow          black           "~v~(~F~N)"                 # collapsed thread with some unread & flagged
#color index         green           black           "~N~v~(~F~N)"               # collapsed thread with unread parent & flagged
#color index         green           black           "~N~v~(~F)"                 # collapsed thread with unread parent, no unread inside, but some flagged
#color index         cyan            black           "~v~(~p)"                   # collapsed thread with unread parent, no unread inside, some to me directly
#color index         yellow          red             "~v~(~D)"                   # thread with deleted (doesn't differentiate between all or partial)
#color index         yellow          default         "~(~N)"                    # messages in threads with some unread
#color index         green           default         "~S"                       # superseded messages
#color index         red             default         "~T"                       # tagged messages
#color index brightred red "~=" # duplicated messages

# Mode line {{{1
# vim:foldmethod=marker:foldlevel=0
