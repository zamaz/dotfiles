set ft=markdown " much better syntax highlighting
let g:asyncrun_open = 0
set concealcursor=ic
nnoremap <buffer> <C-F> :VimwikiSearch 
cnoremap <buffer> <C-F> <C-U>VimwikiSearchTags 
nnoremap <buffer> <C-J> :lnext<CR>
nnoremap <buffer> <C-K> :lprevious<CR>
nnoremap <buffer> <leader>o :lopen<CR>
let &titlestring = "vimwiki - " .expand("%:t")

augroup markdown
	autocmd!
	autocmd BufWritePost <buffer> call Markdown2PDF(0)
augroup END

setlocal concealcursor=c
