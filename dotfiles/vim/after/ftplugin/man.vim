" https://vim.fandom.com/wiki/Using_vim_as_a_man-page_viewer_under_Unix
set nofoldenable
set nonumber
set norelativenumber
set nocursorline
set scrolloff=0
set cmdheight=1
colorscheme wal
if exists(":AirlineTheme")
	execute "AirlineTheme wal"
endif

map q :q<CR>
nnoremap J j
nnoremap K k
nnoremap <silent> b <C-U> :call cursor(line('w0'), 1)<CR>
nnoremap <silent> <SPACE> <C-F> :call cursor(line('w$'), 1)<CR>
nnoremap <silent> j <C-e> :call cursor(line('w$'), 1)<CR>
nnoremap <silent> k <C-y> :call cursor(line('w0'), 1)<CR>
