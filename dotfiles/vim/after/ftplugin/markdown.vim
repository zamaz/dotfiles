" Hard-wrap
setlocal concealcursor=c
setlocal expandtab
let g:asyncrun_open=0 

" Highlighting
hi link mkdLineBreak Normal
hi link mkdListItemLine Identifier
hi link htmlH1 Statement
hi link htmlH2 Function

" Conceal (mark) markdown linebreaks
" id = -1 allows for random IDs which is important e.g. for VimwikiSearch to
" work
" priority = 0 is a workaround for a bug that conceals the most recent match
" from any vim search (https://github.com/vim/vim/issues/2185) a proper
" patch is available upstream (https://github.com/vim/vim/issues/4073#issuecomment-469750382)
if !exists('w:matchId')
	let w:matchId = matchadd('Conceal', '  $', 0, -1, {'conceal': '↲'})
endif

" Mappings
nnoremap <buffer> <leader>O :call Markdown2PDF(1)<CR>
vnoremap <buffer> <leader>O :call Markdown2PDF(1, 'v')<CR>
