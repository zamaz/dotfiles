<VirtualHost 130.183.17.94:80> 
    ServerAdmin     ${1:admin}@mpcdf.mpg.de
    ServerName      ${4:${2:`!v expand("%:t:r")`}.${3:mpcdf.mpg.de}}${5:
    ServerAlias ${6:www}.$4}${4/([^.]+)(\.mpcdf\.mpg\.de)|.*/(?2:
    ServerAlias     $1.rzg.mpg.de)/}

    LogLevel  warn 
    ErrorLog  /var/log/httpd/${2/\./-/g}-error.log 
    CustomLog /var/log/httpd/${2/\./-/g}.log combined

    ${5/\s+ServerAlias\s+(www)\.(.*)|.*/(?1:Redirect 301 \/ https\:\/\/$2\/:# Use RewriteEngine rather than Redirect to account for the different
    # domains (Redirect can only redirect to one domain)
    # Options\:
    #  [NE] - Do not html-encode special chars (backend may have issues with encoding)
    #  [R]  - Issue a redirect to the browser with a 301 status code
    #  [L]  - Don't process any other redirects after this
    RewriteEngine On
    RewriteCond %\{HTTPS\} !=on
    RewriteRule ^\/?(.*) https\:\/\/%\{SERVER_NAME\}\/\$1 [NE,R=301,L])/}
</VirtualHost> 

<VirtualHost 130.183.17.94:443>
    ServerAdmin     $1@mpcdf.mpg.de
    ServerName      $4$5${4/([^.]+).(mpcdf\.mpg\.de)|.*/(?2:
    ServerAlias     $1.rzg.mpg.de:)/}

    LogLevel  warn
    ErrorLog  /var/log/httpd/$2-ssl-error.log
    CustomLog /var/log/httpd/$2-ssl.log combined

	## SSL in the frontend
    SSLEngine on${3/(mpcdf\.mpg\.de)|(.*)/(?2:
    Include                     \/etc\/letsencrypt\/options-ssl-apache.conf)/}
    SSLCertificateFile          ${4/([^.]+)((\.mpcdf)?\.mpg\.de)|(.*)/(?2:\/etc\/pki\/tls\/certs\/$1\/$1$2.crt:\/etc\/letsencrypt\/live\/$1$3\/cert.pem)/}
    SSLCertificateKeyFile       ${4/([^.]+)((\.mpcdf)?\.mpg\.de)|(.*)/(?2:\/etc\/pki\/tls\/private\/$1\/$1$2.key:\/etc\/letsencrypt\/live\/$1$3\/privkey.pem)/}
    SSLCertificateChainFile     ${4/([^.]+)((\.mpcdf)?\.mpg\.de)|(.*)/(?2:\/etc\/pki\/CA\/DFN-PKI\/dfn-pki-2-chain.pem:\/etc\/letsencrypt\/live\/$1$3\/chain.pem)/}

    Protocols h2 http/1.1
    Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains; preload"

	## SSL in the backend
	${7:# Only include this in case of an old backend system not supporting TLSv1.1 or higher.
    # This is the case for e.g. SLES11 using OpenSSL 0.9.8.
    SSLProxyProtocol all -SSLv2 -SSLv3
    SSLHonorCipherOrder on
    SSLProxyCipherSuite ECDHE-ECDSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA
		}
    SSLProxyEngine          ${8:on}
    SSLProxyVerify          none
    SSLProxyCheckPeerCN     off
    SSLProxyCheckPeerName   off
    SSLProxyCheckPeerExpire off

    ## Traffic delegation to backend
    ProxyPreserveHost on
    ProxyRequests     off
    ProxyPass         / http${8/(on|On)|.*/(?1:s)/}://${11:130.183.${9:  }.${10:  }}/
    ProxyPassReverse  / http${8/(on|On)|.*/(?1:s)/}://$11/

    <Proxy *>
        Require all granted
    </Proxy>

    # add some security, see
    # general conf: /etc/httpd/conf.d/mod_security.conf
    # rules:        /etc/httpd/modsecurity.d/activated_rules
    # rules conf:   /etc/httpd/modsecurity.d/crs.conf
    <IfModule mod_security2.c>
        SecRuleEngine DetectionOnly
        SecAuditLog   /var/log/httpd/modsec_audit_$2.log
    </IfModule>
</VirtualHost>
