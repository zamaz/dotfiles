#!/bin/bash${1:
#
# Author: ${2:Amazigh Zerzour}
# Date: `date +"%d %b %Y"`
#
# ${5:`!v expand("%:p:t:r")`} - ${6:Short description}
#
# ${7:Long description}}
