#!/bin/sh

userresources=$HOME/.Xresources
usermodmap=$HOME/.Xmodmap
sysresources=/etc/X11/xinit/.Xresources
sysmodmap=/etc/X11/xinit/.Xmodmap

# merge in defaults and keymaps

if [ -f $sysresources ]; then
    xrdb -merge $sysresources
fi

if [ -f $sysmodmap ]; then
    xmodmap $sysmodmap
fi

if [ -f "$userresources" ]; then
    xrdb -merge "$userresources"
fi

if [ -f "$usermodmap" ]; then
    xmodmap "$usermodmap"
fi

if [ -d /etc/X11/xinit/xinitrc.d ] ; then
 for f in /etc/X11/xinit/xinitrc.d/?*.sh ; do
  [ -x "$f" ] && . "$f"
 done
 unset f
fi

# Remap PgUp and PgDown since I always accidentally hit them when trying to press Left or Right
xmodmap -e 'keycode 112 = Left'
xmodmap -e 'keycode 117 = Right'

# Map CapsLock to ESC for easier vimming and because nobody needs CapsLock
# https://unix.stackexchange.com/questions/199266/how-to-permanently-remap-caps-lock-to-esc-in-x11
xmodmap -e "clear lock" # Without this, CapsLock will still lock caps
xmodmap -e "keycode 66 = Escape NoSymbol Escape"

# Disable red Lenovo mouse button (TrackPoint)
# https://unix.stackexchange.com/questions/32992/thinkpad-disable-trackpoint
xinput set-prop "ETPS/2 Elantech TrackPoint" "Device Enabled" 0
