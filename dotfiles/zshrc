##################################################
# zshrc - zsh configuration file
##################################################

# FUNCTIONS {{{1
FUNC_DIR="$HOME/.config/zsh/functions"
[ -f "$FUNC_DIR/git.zsh" ] && . "$FUNC_DIR/git.zsh"
[ -f "$FUNC_DIR/misc.zsh" ] && . "$FUNC_DIR/misc.zsh"
[ -f "$FUNC_DIR/spectrum.zsh" ] && . "$FUNC_DIR/spectrum.zsh"

function echo_blank() {
    echo
}

function tags() {
    git tag --sort -version:refname |
        fzf-tmux --multi --preview-window right:70% \
        --preview 'git show --color=always {} | head -'$LINES
}

function clone() {
    # This function allows any git clone options to be passed but assumes that
    # the last argument is either the URL or the path of the local repository.
    # The cut argument is used to correctly parse git 'insteadOf' shortcuts,
    # e.g.:
    # clone aur:xautounlock
    git clone "$@" && 
        clone_path="$(echo "$_" | cut -d: -f2-)" &&
        cd "$(basename "$clone_path" .git)"
    }

function ssh() {
    # Turn on vpn if not at work
    ip a | grep -q 130.183.2 || check-vpn || vpn

    # Add ssh keys to ssh agent if not done yet
    ssh-init silent || exit 1

    # Try to execute the function "zama" on the remote host each time ssh is
    # invoked. The zama function starts up zsh and the ssh session will last
    # until zsh is terminated. In case zsh is not installed on the remote
    # system or the zama function does not exist a bash session is started
    # instead.
    #
    # If ssh is invoked with more than one argument, don't launch the remote
    # environment
    if [[ "${#@}" -eq 1 ]] && [ $1 != 'nas' ]; then
        /usr/bin/ssh -t "$@" "command -v zsh >/dev/null && $USER || bash -l"
    else
        /usr/bin/ssh "$@"
    fi
}

function sshh() {
    # Turn on vpn if not at work
    ip a | grep -q 130.183.2 || check-vpn || vpn up

    # Add ssh keys to ssh agent if not done yet
    ssh-init silent || exit 1

    /usr/bin/ssh "$@"
}

function diffdir() {
    if [ ! -d $1 ] || [ ! -d $2 ]; then
        echo "Invalid paths"
    else
        diff <(echo "$(/bin/ls $1)") <(echo "$(/bin/ls $2)")
    fi
}

function which_term(){
    echo $(ps -p $(ps -p $$ -o ppid=) -o comm=);
}

# Higlight IPs in the output of `ip a`
function ipa(){
    ip a | grep --color -E "^[0-9]+: [^ ]+|(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/[0-9]{1,3}|$"
}

# List only interface names and associated IPs
function ipaa() {
    ip a | grep -oE "^[0-9]+: [^ ]+|(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/[0-9]{1,3}" | awk 'match($0, /(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\/[0-9]{1,3}/){printf "%-20s %s\n",p,$0}{p=$2}'
}

function trash-restore(){
    if [[ -z "$1" ]]; then
        echo "Usage: $(basename "$0") <path>"
        return 1
    fi

    if [ -f "$1" ]; then
        echo "Target file exists"
        return 1
    fi

    TRASH="$HOME/.local/share/Trash/files"
    INFO="$HOME/.local/share/Trash/info"

    infofile="$(grep -l "$1" "$INFO"/*)"
    fname="$(basename "$1")"
    dname="$(dirname "$1")"
    delpath="$TRASH/$fname"
    if [ -f "$delpath" ]; then
        mkdir -p "$dname" && mv "$delpath" "$1" && rm "$infofile" && echo "File restored to $1"
    else
        echo "Could not restore $delpath: No such file"
    fi
}

function export_ssh_vars(){
    export SSH_ENV="$HOME/.ssh/env"
    [ -f "$SSH_ENV" ] && \
        . "${SSH_ENV}" > /dev/null && \
        export $(cut -d= -f1 "$SSH_ENV")
}

function enable_beam_cursor() {
    term="$(which_term | awk '{print $1}')"
    case $term in
        konsole)
            printf "\e]50;BlinkingCursorEnabled=1\x7"
            printf "\e]50;CursorShape=1\x7"
            ;;
        *) printf '\e[5 q'
    esac
    PS1=${PS1//➜}
}

function enable_block_cursor() {
    term="$(which_term | awk '{print $1}')"
    case $term in
        konsole)
            printf "\e]50;BlinkingCursorEnabled=0\x7"
            sleep 0.1
            printf "\e]50;CursorShape=0\x7"
            ;;
        *) printf '\e[2 q'
    esac
    PS1=${PS1/➜/}
}

# ENVIRONMENT VARIABLES {{{1
export PATH=$HOME/.gem/ruby/2.6.0/bin:$HOME/.yarn/bin:$HOME/.local/bin:$HOME/mpcdf/bin:$HOME/bin:$HOME/bin/i3-workspaces:/usr/local/bin:$PATH

if command -v ag >/dev/null; then
    export FZF_DEFAULT_COMMAND='ag -g ""'
fi
FZF_DEFAULT_OPTS="--color bg:-1"

# Set up virtualenvwrapper
if which virtualenvwrapper.sh >/dev/null; then
    export VIRTUALENVWRAPPER_PYTHON=`which python3`
    export WORKON_HOME=$HOME/.virtualenvs
    #export PROJECT_HOME=$HOME/Devel
    source "$(which virtualenvwrapper.sh)"
fi

export_ssh_vars

# Don't treat special characters as if they are part of words
# This makes forward_word stop at special charactes which is useful when
# navigating autosuggestions. vi mode parses words differently altogether and
# is not affected by this (see man zshall).
export WORDCHARS=

# This is necessary for virtualenvwrapper to work properly. This global
# variable would be set wrong in the script itself.
export VIRTUALENVWRAPPER_SCRIPT=/usr/bin/virtualenvwrapper.sh

export GOPATH=/home/zama/go

# Enable 256 colors
export TERM="xterm-256color"

# Open st as i3-sensible-terminal
export TERMINAL="st"

# Use qutebrowser as default
# Use xdg-settings set default-web-browser qutebrowser.desktop instead
# for a more consistent result
export BROWSER=qutebrowser

export TASKRC=~/.config/taskrc

# XDG specification
# https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
export XDG_CURRENT_DESKTOP=i3
export XDG_DATA_DIRS="/usr/local/share/:/usr/share/:$HOME/.local/share/"

# Prevent ranger from loading default config
export RANGER_LOAD_DEFAULT_RC=FALSE

# Default editor
export VISUAL=vim
export EDITOR="$VISUAL"

# Default browser for rtv
export RTV_BROWSER="surf"

# Reduce vim mode switching lag to 0.1s
export KEYTIMEOUT=1

# Colorful less
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# Make less show location within file
#export MANPAGER='less -s -M +Gg'

# Use vim as manpager
export MANPAGER="/bin/sh -c \"unset MANPAGER;col -b -x | \
    vim -R -c 'set ft=man nomod nolist' -\""

# ZSH CONFIG {{{1
if [ -d "$HOME/.oh-my-zsh" ]; then
    export ZSH="$HOME/.oh-my-zsh"
    ## Load oh-my-zsh{{{
    ZSH_THEME="fino-wal"
    HYPHEN_INSENSITIVE="true"
    ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=3'
    #ENABLE_CORRECTION="true"
    #COMPLETION_WAITING_DOTS="true"

    # Plugins
    plugins=(
        git
        python
        vi-mode
        common-aliases
        zsh-autosuggestions
        web-search
        zsh-syntax-highlighting
        pip
        command-not-found
    )

    [ -f $ZSH/oh-my-zsh.sh ] && source $ZSH/oh-my-zsh.sh

    # }}}
else
    ## Completion{{{2
    autoload -Uz compinit
    compinit

    setopt complete_in_word
    setopt always_to_end

    # Arrow-keys driven completion
    zstyle ':completion:*' menu select

    # Auto-detect new binaries
    zstyle ':completion:*' rehash true

    # Make completion case-insensitive and allow for completion to the left (bar
    # -> foobar)
    # Also make hyphens and underscores interchangeable
    zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

    # Autocompletion of command line options for aliases
    setopt COMPLETE_ALIASES

    ## Custom prompt{{{2
    fpath=("$HOME/.config/zsh/themes" "$fpath[@]") # Needs to come before promptinit
        autoload -Uz promptinit
        setopt promptsubst
        promptinit
        prompt fino-wal # ~/.zprompts/fino-wal.zsh

    # Allow leaving out cd to change directories
    setopt autocd

    # Add ~ in front of invalid cd paths
    setopt cdablevars

    zmodload zsh/zpty
    ZSH_AUTOSUGGEST_STRATEGY=(history completion)
    ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=3'
    ZSH_AUTOSUGGEST_USE_ASYNC=true

    ## History{{{2
    HISTSIZE=10000
    SAVEHIST=10000
    HISTFILE=~/.zsh_history
    ## Retain recent directories stack
    ## List with 'dirs -v', cd using cd -<NUM>
    autoload -Uz add-zsh-hook

    DIRSTACKFILE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh/dirs"
    DIRSTACKDIR="$(dirname $DIRSTACKFILE)"
    [ ! -d "$DIRSTACKDIR" ] && mkdir -p "$DIRSTACKDIR" 

    # Load dirstack when starting shell
    if [[ -f "$DIRSTACKFILE" ]] && (( ${#dirstack} == 0 )); then
        dirstack=("${(@f)"$(< "$DIRSTACKFILE")"}")
        # Comment out to auto-cd into last directory
        #[[ -d "${dirstack[1]}" ]] && cd -- "${dirstack[1]}"
    fi

    chpwd_dirstack() {
        print -l -- "$PWD" "${(u)dirstack[@]}" > "$DIRSTACKFILE"
    }
add-zsh-hook -Uz chpwd chpwd_dirstack

DIRSTACKSIZE='20'

setopt AUTO_PUSHD PUSHD_SILENT PUSHD_TO_HOME

    ## Remove duplicate entries
    setopt PUSHD_IGNORE_DUPS

    ## Ignore duplicates when going through the history
    setopt HIST_IGNORE_ALL_DUPS

    ## This reverts the +/- operators.
    setopt PUSHD_MINUS

    # Only show history matching what has already been typed
    autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
    zle -N up-line-or-beginning-search
    zle -N down-line-or-beginning-search

    [[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
    [[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

    ## Misc {{{2
    # Make it possible to exit shell with Ctrl+d if command line is not empty 
    exit_zsh() { exit }
    zle -N exit_zsh
    bindkey '^D' exit_zsh	

    # Enable zmv for more powerful file renaming
    autoload zmv

    ## Source plugins {{{2
    HIGHLIGHTING=/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
    AUTOSUGGEST=/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
    SUBSTR_SEARCH=/usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
    CMD_NOT_FOUND=/usr/share/doc/pkgfile/command-not-found.zsh
    SYS_CLIP="$HOME/.zsh/plugins/zsh-system-clipboard/zsh-system-clipboard.zsh"
    [ -f "$HIGHLIGHTING" ] && . "$HIGHLIGHTING"
    [ -f "$AUTOSUGGEST" ] && . "$AUTOSUGGEST"
    [ -f "$SUBSTR_SEARCH" ] && . "$SUBSTR_SEARCH"
    [ -f "$CMD_NOT_FOUND" ] && . "$CMD_NOT_FOUND"
    [ -f "$SYS_CLIP" ] && . "$SYS_CLIP"
    #}}}
fi

# ALIASES {{{1
# Config files {{{2
alias zshrc="vim ~/.zshrc && source ~/.zshrc"
alias vimrc="vim ~/.vimrc"
alias polyrc="vim ~/.config/polybar/config"
alias i3rc="vim ~/.config/i3/config && i3-msg restart"
alias conkyrc="vim -O ~/.conkyrc ~/.conkyrc_clock"
alias ohmyrc="vim ~/.oh-my-zsh"
alias dunstrc="vim ~/.config/dunst/dunstrc && pkill dunst"
alias comptonrc="vim ~/.config/compton.conf && launchcompton"
alias xrc="vim ~/.Xresources && xrdb ~/.Xresources"
alias rofirc="vim ~/.config/rofi/config"
alias rangerrc="vim ~/.config/ranger/rc.conf"
alias quterc="vim ~/.config/qutebrowser/config.py"
alias mpdrc="vim ~/.config/mpd/mpd.conf"
alias ncmprc="vim ~/.config/ncmpcpp/config"
alias muttrc="vim ~/.config/mutt/muttrc"
alias gitrc="vim ~/.gitconfig"
alias strc="vim ~/sourcecode/st/config.def.h"
alias sshrc="vim ~/.ssh/config"
alias gitrc="vim ~/.gitconfig"
alias gitignorerc="vim ~/.gitignore_global"

# Other {{{2
if [ -f /opt/coreutils/bin/ls ]; then
    LS=/opt/coreutils/bin/ls
elif (( $+commands[ls-icons] )); then
    LS="ls-icons"
else
    LS=/usr/bin/ls
fi

alias ls="$LS --color --group-directories-first -NHL"
alias l="ls -lh"
alias la="ls -lAh"
alias lsa="ls -Ah"
alias vi=vim
alias mmv='noglob zmv -W' # Make something like this possible: mmv *.c.orig orig/*.c
alias grep='grep --color'
# Ignore /usr/bin/weather provided by expect package
alias weather="$HOME/bin/weather"
alias sgs9="sudo mkdir -p /tmp/sgs9 && sudo sshfs 192.168.2.162:/storage/emulated/0 /mnt/sgs9 -p 2222 -o allow_other && cd /mnt/sgs9"
alias workon="source virtualenvwrapper.sh && workon"
alias list-fonts="fc-list | cut -d: -f2- | sort | less"
alias okular="okular 2>/dev/null"
alias todo="todoist --color --indent"
alias tdl="todoist list --color --indent"
alias tdc="todoist close"
alias token="$HOME/mpcdf/projects/token-tracker/token-tracker.py"
alias dog='pygmentize -g -O style=colorful'
# Hide mysql welcome message but keep table boundaries
# https://serverfault.com/questions/436474/is-it-possible-to-change-the-mysql-welcome-message
alias mysql='mysql --silent --table' 
alias wiki='cd ~/rzgdatashare/vimwiki && vim +VimwikiIndex'
alias wints='~/mpcdf/bin/rdp/wints&'
alias updatehosts='sudo git -C /opt/hosts checkout -- . && sudo git -C /opt/hosts pull && sudo python3 /opt/hosts/updateHostsFile.py --auto --replace --flush-dns-cache --extensions fakenews gambling porn'
# S/MIME email signing doesn't work with neomutt for some reasom
#alias mutt='neomutt'
alias rtv='rtv --enable-media'
alias reddit=ttrv
alias signal="TMPDIR=$HOME/.cache/signal signal-desktop"
alias -g ...="../.."
alias -g ....="../../.."
if (( $+commands[nvim] )); then
    alias vim='nvim'
fi
if (( $+commands[trash] )); then
    alias rm=trash
    alias rmf=/bin/rm
fi
# }}}

# KEYBINDINGS {{{1
# Enable vim keybindings
bindkey -v
export KEYTIMEOUT=1

# Open vim with ctrl-e
autoload edit-command-line
zle -N edit-command-line
bindkey -M vicmd '^e' edit-command-line

# Accept autosuggestions
bindkey '^ ' forward-word
bindkey '^l' autosuggest-execute

# Enable history search with Ctrl-R. This will get overriden if fzf is installed
bindkey '^R' history-incremental-search-backward

# Search history based on text already entered at prompt using vim-like
# keybindings
bindkey "^K" history-beginning-search-backward
bindkey "^J" history-beginning-search-forward

# Make backspace work after using vi normal mode
bindkey "^?" backward-delete-char

# Git commands as vim keybindings
bindkey -M vicmd -s gs 'i^ugit status^M'
bindkey -M vicmd -s ga 'i^ugit add \t'
bindkey -M vicmd -s gc 'i^ugit commit^M'
bindkey -M vicmd -s gl 'i^ugit lola^M'
bindkey -M vicmd -s gu 'i^ugit pull^M'
bindkey -M vicmd -s gp 'i^ugit push^M'
bindkey -M vicmd -s gf 'i^ugit fetch^M'
bindkey -M vicmd -s gd 'i^ugit diff^M'
bindkey -M vicmd -s gr 'i^ugit rebase \t'
bindkey -M vicmd -s gC 'i^ugit checkout \t'
bindkey -M vicmd -s gA "i^uvim +Agit -c 'map q :quit!<CR>'^M"

# AUTORUN {{{1
# MISC {{{1
# Add a blank line after the prompt and after the command output
# Check if the function is already in the arrays to avoid too many 
# blank lines after sourcing this file more than once
if [[ ! ${preexec_functions[(r)echo_blank]} == echo_blank ]] ; then
    preexec_functions+=echo_blank
fi
if [[ ! ${precmd_functions[(r)echo_blank]} == echo_blank ]] ; then
    precmd_functions+=echo_blank
fi
if [[ ! ${precmd_functions[(r)enable_beam_cursor]} == enable_beam_cursor ]] ; then
    precmd_functions+=enable_beam_cursor
fi

# Print Life is Strange quote if hella mode is active
if [ -f /tmp/hella ] && [ "$(cat /tmp/hella)" = "1" ]; then
    shuf -n1 ~/.conky/lis-quotes.txt
    export PROMPT_CHAR='✎'
else
    export PROMPT_CHAR='➜'
fi

# Start gpg-agent
# See man gpg-agent
GPG_TTY=$(tty)
export GPG_TTY

# Disable Ctrl-S terminal freeze
stty -ixon

# Change cursor shape when switching vi mode
zle -N zle-keymap-select
zle-keymap-select () {
case $KEYMAP in
    vicmd) enable_block_cursor;;
    viins|main) enable_beam_cursor;;
esac
zle reset-prompt
}
enable_beam_cursor

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
# MODE LINE {{{1
# vim:foldmethod=marker:foldlevel=0
