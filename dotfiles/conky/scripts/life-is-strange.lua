require 'cairo'
require 'imlib2'

function conky_butterfly()
	path = '~/.conky/img/life-is-strange/but'
	nim = 9
	conky_gif(nim, path)
end

function conky_gif(nim, path, x, y, w, h)
	if conky_window == nil then
		return
	end

	local cs = cairo_xlib_surface_create(conky_window.display,
																			 conky_window.drawable,
																			 conky_window.visual,
																			 conky_window.width,
																			 conky_window.height)
	cr = cairo_create(cs)

	updates=tonumber(conky_parse('${updates}'))
	count=(updates %nim) + 1

	if string.len(count) == 1 then 
		count=("00"..count)
	elseif string.len(count) == 2 then 
		count=("0"..count)
	end

	show_image = path..count
	image({x=x, y=y, w=w, h=h, file=show_image, decache=1})
	cairo_destroy(cr)
	cairo_surface_destroy(cs)
	cr = nil
	return ""
end

function image(im)
	local x=(im.x or 0)
	local y=(im.y or 0)
	local w=(im.w or 50)
	local h=(im.h or 50)
	local file=tostring(im.file)
	local decache=im.decache or 0

	if file==nil then
		print("set image file")
	end

	local show = imlib2.image.load(file)

	if show == nil then
		return
	end

	imlib_context_set_image(show)
	if tonumber(w)==0 then 
		width=show.get_width() 
	else
		width=tonumber(w)
	end

	if tonumber(h)==0 then 
		height=show.get_height() 
	else
		height=tonumber(h)
	end

	imlib_context_set_image(show)
	local scaled=imlib_create_cropped_scaled_image(0,
																								 0,
																								 imlib_image_get_width(),
																								 imlib_image_get_height(),
																								 width,
																								 height)
	if decache==1 then
		imlib_free_image_and_decache()
	else
		imlib_free_image()
	end
	imlib_context_set_image(scaled)
	imlib_render_image_on_drawable(x,y)
	imlib_free_image()
end
