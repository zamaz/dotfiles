#!/usr/bin/python3
#
# Lists details about docker environment. Supposed to be used with conky.
#
# Credit: @erosb
# https://erosb.github.io/post/docker-containers-in-conky/

import math
import docker
client = docker.from_env()

print("\nImages")
print("{:14} {:20} {:9} {:4}".format('ID', 'Tag', 'Os', 'Size'))
for img in client.images.list():
    os = img.attrs['Os']
    iid = img.attrs['Id'].split(':')[1][:12]
    tag = img.attrs['RepoTags'][0]
    size = math.ceil(img.attrs['Size']/10**6)
    print("{:14} {:20} {:9} {:4}MB".format(iid, tag, os, size))

print("\nNetworks")
print("{:14} {:20} {:9}".format('ID', 'Name', 'Gateway'))
for net in client.networks.list():
    name = net.attrs['Name']
    nid = net.attrs['Id'][:12]
    try:
        gateway = net.attrs['IPAM']['Config'][0]['Gateway']
    except IndexError:
        gateway = ''
        print("{:14} {:20} {:9}".format(nid, name, os))

print("\nContainers")
print("{:14} {:20} {:9}".format('ID', 'Name', 'Status'))
for cont in client.containers.list():
    cid = cont.attrs['Id'][:12]
    name = cont.attrs['Name']
    status = cont.attrs['State']['Status']
    print("{:14} {:20} {:9}".format(cid, name, status))
