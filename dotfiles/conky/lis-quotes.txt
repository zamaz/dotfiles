"Now why don't you go fuck your selfie." - Victoria Chase
"After 5 years you're still Max Caulfield." - Chloe Price
"Boo yaa! Get it? Boo ya? Like I'm a scary punk ghost." - Chloe Price
"Max, I'm in a nightmare and I can't wake up, unless I put myself to sleep." - Kate Marsh
"I wish I could stay in this moment forever. I guess I actually can now, but then it wouldn't be a moment." - Max Caulfield
"Max, never Maxine." - Max Caulfield
"Release the kra-can." - Max Caulfield
"With great power comes great bullshit." - Max Caulfield
"Always take the shot." - Mark Jefferson
"Max you are not crazy. You are not dreaming. It's time to be an everyday hero." - Max Caulfield
"Chloe, my powers might not last." - Max Caulfield
"That's okay. We will. Forever." - Chloe Price
"I'm so glad you're my partner in crime" - Max Caulfield
"As long as you're my partner in time." - Chloe Price
"MaxGyver strikes again!" - Max Caulfield
"I pledge allegiance to Max and the power for which she stands" - Chloe Price
"When a door closes, a window opens... Or, something like that." - Max Caulfield
"Ready for the mosh pit, shaka brah?" - Max Caulfield
"This shit-pit has taken everyone I've ever loved... I'd like to drop a bomb on Arcadia Bay and turn it to fucking glass." - Chloe Price
"Since you're the mysterious superhero, I'll be your loyal chauffeur and companion!" - Chloe Price
"Max, It's... It's Nathan. I just wanted to say... I'm sorry. I didn't want to hurt Kate or Rachel, or... didn't wanted to hurt anybody. Everybody... used me. Mr. Jefferson... is coming for me now. All this shit will be over soon. Watch out, Max... He wants to hurt you next. Sorry." - Nathan Prescott
"Like time, my texts are infinite." - Warren Graham
"If that tornado came right now, I would just sit here and watch for a while." - Max Caulfield
"The past... within the past. Ugh. Am I pushing myself too hard?" - Max Caulfield
"I'll be in the TARDIS getting my Delorean ready." - Warren Graham
"The slightly unconscious model is often the most open and honest. No vanity or posing, just pure expression." - Mark Jefferson
"Listen, whatever happens, I want you to be strong. Even if you feel like I wasn't there for you because I will never abandon you Chloe. I'll always have your back. Always." - Max Caulfield
"This is bullshit. Fuck you, door!" - Chloe Price
"You eat like a pig, go try the floor." - Max Caulfield
"I was eating those beans!" - Frank Bowers
"Max, do your powers include mind reading? Or did you just rewind because I tried to steal the chair? I'm confused." - Chloe Price
"And with the leftover dough, I'll take you on a road trip to Portland for the day. We'll stock up on tats, beer, weed, and donuts." - Chloe Price
"...and books from Powells." - Max Caulfield
"And strip club! Kidding... but you never know." - Chloe Price
"Check out mad max, ready to fucking thrash. I'm so hardcore." - Max Caulfield
"I gave most of the flowers to other patients because they need them more than me, I'm keeping the ballons though." - Kate Marsh
"That's another dollar for the swear jar" - Chloe Price
"You show up here again, and I'll show you real trouble" - David Madsen
"Speak of the devil." - Joyce Price
"And Max Caulfield, don't you forget about me." - Chloe Price
"Never." - Max Caulfield
"You're the most amazing person I ever met." - Warren Graham
"Just because I'm mentally ill, doesn't mean I deserve to die, Max!" - Nathan Prescott
"You don't know who the fuck I am or who you're messing around with!" - Nathan Prescott
"Don't ever tell me what to do. I'm so sick of people trying to control me!" - Nathan Prescott
"Don't be scared... You own this school... If I wanted, I could blow it up... You're the boss..." - Nathan Prescott
"I changed fate and destiny so much that I actually did alter the course of everything. And all I really created was just death and destruction!" - Max Caulfield
"I feel like I took this shot a thousand years ago." - Max Caulfield
"Kate, your life is still yours. And we can get through this together... Let me help." - Max Caulfield
"Okay, so you're not the goddamn Time Master, but you're Maxine Caulfield... And you're amazing." - Chloe Price
"There's so many more people in Arcadia Bay who should live.. way more than me..." - Chloe Price
"Max you finally came back to me this week, and you did nothing but show me your love and friendship. You made me smile and laugh, like I haven't done in years." - Chloe Price
"Wherever I end up after this... in whatever reality... all those moments between us were real, and they'll always be ours." - Chloe Price
"No matter what you choose, I know you'll make the right decision." - Chloe Price
"I don't want to see or hear you again, Max. You've hurt me and my family enough." - David Madsen
"I'm saying that being with you again has been so special. I just wanted to feel like when we were kids running around Arcadia Bay... and everything was possible. And you made me feel that way today. I want this time with you to be my last memory. Do you understand?" - Chloe Price
"We got no time for this shit. Come on, Max." - Chloe Price
"You better not rewind and take that kiss back. You know I'm hotter than those Seattle art-holes, right?" - Chloe Price
"Okay, I see you're a geek now with a great imagination, but this isn't an anime or a video game. People don't have these powers, Max." - Chloe Price
"Why look, an otter in my water!" - Chloe Price
"I'm so proud of you for following your dreams. Don't forget about me." - Chloe Price
"Yes, I am a scientist! Fuck!" - Warren Graham
"You're right. We hella deserve this. Splish splash." - Max Caulfield
"Did you just say 'hella'? I think I'm a good bad influence on you." - Chloe Price
"Amazeballs! I literally got chills all over my neck." - Chloe Price
"I hope you checked the perimeter, as my step-ass would say. Now, let's talk bidness" - Chloe Price
"Beautiful, I don't give a shit. The world is ending, cool." - Chloe Price
"Get that gun away from me, psycho!" - Chloe Price
"No one has to see me dance. Plus, you don't wanna watch the old hipster trying to keep up with the kids!" - Mark Jefferson
"Now you're totally stuck in the retro-zone. Sadface." - Victoria Chase
"Even angels need angels." - Kate Marsh
"Oh man, are you cereal?" - Max Caulfield
"Nerd alert! My stepdad has a fully stocked garage. And he actually is a tiny tool." - Chloe Price
"You're so obvious. And I still get freaked out by that movie, so stop." - Max Caulfield
"Your power is changing everything, Max. Especially you. I can already tell." - Chloe Price
"So it's time to start moving forward in time." - Chloe Price
"And we're obviously connected since without me you would have never discovered your power, right?" - Chloe Price
"Absolutely. You make me feel like I know what I'm doing..." - Max Caulfield
"Every great artist gets rejected before they get accepted. So you have to enter a photo." - Chloe Price
"Put this on and let your inner punk-rock girl come out! You can afford to take chances!" - Chloe Price
"Stop second guessing yourself, Max!" - Chloe Price
"Yes, we'd be tearing up the highway. And you'd probably want me to kiss you again..." - Max Caulfield
"Oh, we could cruise everywhere in this bad boy. Can you see us heading down the coast to Big Sur and beyond?" - Chloe Price
"Can we build another pirate fort and keep the world out?" - Max Caulfield
"No one would remember your punk ass anyway!" - Nathan Prescott
"I do not have to take this kind of interrogation. Not from you punks!" - David Madsen
"Whateverthefuck." - Nathan Prescott
"Max, you're smart to be a loner here." - Dana Ward
"Max thought we were going to be buds, fucking ha-ha." - Victoria Chase
"I always feel like I have to overcompensate. For what I have no clue. I'm only here to become a photographer, not president." - Victoria Chase
"Max you'll be here too right?" - William Price
"She's never leaving me!" - Chloe Price
"That makes all of us." - William Price
"You're like the smartest, most talented person I've ever known." - Chloe Price
"We left a skid mark on Blackwell last night." - Chloe Price
"And you make me feel like I have a reason for still being in Arcadia Bay." - Chloe Price
"I'm kinda over humanity right now. Sorry to be a drama queen." - Kate Marsh
"Come on, slowpoke" - Chloe Price
"Because we're yapping instead of attacking each other, otter-versus-shark style... I think I've had my pool experience for the year, let's jet." - Chloe Price
"I'll just rewind and harpoon you. Otter's revenge!" - Max Caulfield
"Max Caulfield! Taking a break after taking Seattle by storm... We thought we'd never see you again, after you left for the big city." - William Price
"Drink up buttercup." - Max Caulfield
"Have a seat Pete." - Chloe Price
"Look, the worst thing you can do is treat me like a baby." - Chloe Price
"This is seriously the best view of the sunset. What do photographers call that?" - Chloe Price
"The Golden Hour" - Max Caulfield
"See? Without you here, I'd have no clue. Bet you could take some amazing shots." - Chloe Price
"At least I'm alive here with you." - Chloe Price
"Not now, Max. I'm contemplating shit." - Alyssa Anderson
"You're kind and sensitive, when you don't even have to be." - Max Caulfield
"It's the power of best friendship, I know how you roll." - Max Caulfield
"I can't trust anybody again. everybody pretends to care until they don't. Even you!" - Chloe Price
"Do you know what's it's like to wait for your father to come home when you're a kid and he never does?" - Chloe Price
"Why does everybody in my life let me down? My dad gets killed, you bail on me for years, my mother gloms onto step-fucker now rachel betrays me." - Chloe Price
"Chloe, Rachel is missing. nobody betrayed you." - Max Caulfield
"Bullshit, who hasn't?! Fuck everybody!" - Chloe Price
"Now we can go to the drive-in. There's one in Newberg, just 60 miles away." - Warren Graham
"You saved me again. Crazy. Now we're totally bonded for life!" - Chloe Price
"Aren't you glad I took you away to a nice quiet desolate spot?" - Chloe Price
"Welcome home Max." - Chloe Price
"Must protect my precious, so Max never has to chase it down again." - Max Caulfield
"I can tell everybody Nathan Prescott is a punk ass who begs like a little girl and talks to himself." - Chloe Price
"Looking sick Max! A couple tats, some piercings and we'll make a thrasher out of you yet!" - Chloe Price
