#!/bin/sh
# This script is called by cron and needs to have its commands specified with the full path
# DISPLAY needs to be set in order for notify-send to work
export DISPLAY=:0

BAT_STATUS="$(/bin/cat /sys/class/power_supply/BAT0/status)"
BAT_LEVEL="$(/bin/cat /sys/class/power_supply/BAT0/capacity)"
# Holds value of charge level last notified about
BAT_LVL_FILE=/tmp/bat_lvl_notice 
# echo 100 is necessary in case the laptop starts out with e.g. 10%
# and no notice has been given for higher charge levels. This happens
# for example after waking up after a very long time
CUR_NOTICE="$(/bin/cat $BAT_LVL_FILE 2>/dev/null || echo 100)"

[ $BAT_STATUS = 'Charging' ] && echo "Battery is charging" && exit

if [ $BAT_LEVEL -gt 50 ]; then
	echo "Battery is at $BAT_LEVEL%"
	rm -f $BAT_LVL_FILE

elif [ $BAT_LEVEL -lt 5 ] && [ $CUR_NOTICE -gt 5]; then
	/home/zama/bin/power_saving strict
	/usr/bin/notify-send -u critical "Battery is critically low"
	echo 5 > $BAT_LVL_FILE

elif [ $BAT_LEVEL -lt 10 ] && [ $CUR_NOTICE -gt 10 ]; then
	/home/zama/bin/power_saving normal
	/usr/bin/notify-send -u critical "Battery is at 10%"
	echo 10 > $BAT_LVL_FILE

elif [ $BAT_LEVEL -lt 25 ] && [ $CUR_NOTICE -gt 25 ]; then
	/home/zama/bin/power_saving normal
	/usr/bin/notify-send -u normal "Battery is at 25%"
	echo 25 > $BAT_LVL_FILE

elif [ $BAT_LEVEL -lt 50 ] && [ $CUR_NOTICE -gt 50 ]; then
	/usr/bin/notify-send -u low "Battery is at 50%"
	echo 50 > $BAT_LVL_FILE
fi
