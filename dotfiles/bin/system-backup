#!/bin/bash
USB_LABEL="system-backups"
USB_NAME="System Backups"
MNT_PATH="/mnt/recoverystick"
USAGE="Usage: system-backup OPTION \n
OPTIONS:\n
\t create PATH \t\t PATH is the absolute path to the backup destination\n
\t restore PATH \t\t PATH is the absolute path to a partition block device (e.g. /dev/sda1)"

if [ ! "$EUID" == "0" ]; then
  echo "Please run as root"
  exit 1
fi

case "$1" in
	help )
		echo -e $USAGE
		exit 0
		;;
	create )
		if [ "$2" == "usb" ]; then
			USB_PATH="$(blkid | grep "$USB_NAME" | cut -d: -f1)"
			if [ ! -b "$USB_PATH" ]; then
				echo "$USB_PATH does not seem to be a valid block device"
				exit 1
			else
				# Check if device is encrypted
				if [ ! -z "$(blkid | grep "$USB_NAME" | grep 'TYPE="crypto_LUKS"')" ]; then
					echo "USB device is encrypted"
					if [ -e /dev/mapper/backup ]; then
						echo "Device is already accessible"
					else
						cryptsetup luksOpen "$USB_PATH" backup || \
							{ echo 'Failed to decrypt device'; exit 1; }
					fi
					USB_PATH=/dev/mapper/backup
				fi
				echo "Backing up to USB device \"$USB_NAME\" ($USB_PATH)"
				mkdir -p "$MNT_PATH"
				MOUNTED_DEVICE="$(mount | grep "$MNT_PATH" | cut -d' ' -f1)"

				# Unmount if wrong device mounted
				if [ ! -z "$MOUNTED_DEVICE" ] && [ ! "$MOUNTED_DEVICE" == "$USB_PATH" ]; then
					echo "Unmounting wrong device"
					umount "$MNT_PATH"
				fi

				# Mount if not already mounted
				if [ ! "$MOUNTED_DEVICE" == "$USB_PATH" ]; then
					echo "mount -o remount,sync "$USB_PATH" "$MNT_PATH""
					mount "$USB_PATH" "$MNT_PATH" || { echo "Failed to mount usb device"; exit 1; }
					echo "Mounted USB device at $MNT_PATH"
				fi

				BKP_PATH="$MNT_PATH"
			fi
		elif [ -z "$2" ] || [ ! -d "$2" ]; then
			echo "Please provide a valid path to the backup folder"
			echo -e $USAGE
			exit 1
		else
			BKP_PATH="$2"
		fi
		SUBFOLDER="backup_$(date '+%Y-%m-%d_%H-%M' )"
		FULL_PATH="$BKP_PATH/$SUBFOLDER"
		echo "Creating full system backup at $FULL_PATH"
		mkdir -p $FULL_PATH
		rsync -aAXHSx --info=progress2 --exclude={"/dev/*","/proc/*","/cdrom/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found/*","/afs/*","/home/**/.thunderbird/*","/home/**/.cache/*","/home/**/rzgdatashare/*","/sys/*","$BKP_PATH/*"} / "$FULL_PATH"
		if $?; then
			echo "Backup successfully created!"
			exit 0
		else
			echo "Backup failed!"
			exit 1
		fi
		;;
	restore )
		DEV_NAME="$(echo $2 | rev | cut -d/ -f1 | rev)"
		if [ -z "$2" ] || [ ! -b "$2" ]; then
			echo "Please provide a valid path to the destination partition"
			echo -e $USAGE
			exit 1
		elif [ -z "$(lsblk -o name -l | grep $DEV_NAME)" ]; then
			echo "$2 is not a valid partition"
			exit 1
		fi
		echo "System restore not implemented yet"
		#echo "Restoring system backup to $2"
		;;
esac
