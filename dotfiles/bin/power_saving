#!/bin/sh
# This script is indirectly called by cron so all commands nedd to be called
# using the full path
DIM_FILE=/tmp/dim_state
DIM_NORMAL=50
DIM_STRICT=20
#CUR_BRIGHTNESS="$(echo "$(/usr/bin/xbacklight) - 2" | /usr/bin/bc)"
CUR_BRIGHTNESS=$(/usr/bin/xbacklight)
echo "Current brightness: $CUR_BRIGHTNESS"

usage() { echo "Usage: power_saving off|normal|strict"; exit 1; }

off() {
	[ -f $DIM_FILE ] || { echo "Power saving is already off"; exit; }
	DIM_LVL_PREV="$(/usr/bin/cut -d'.' -f1 "$DIM_FILE")"
	/bin/rm -f $DIM_FILE

	[ "$(echo "$CUR_BRIGHTNESS < $DIM_LVL_PREV" | /usr/bin/bc)" -eq 1 ] || exit
	echo "Restoring original brightness ($DIM_LVL_PREV%)"
	/usr/bin/xbacklight -set "$DIM_LVL_PREV"
}

normal() {
	[ "$(echo "$CUR_BRIGHTNESS > $DIM_NORMAL" | /usr/bin/bc)" -eq 1 ] || exit
	echo "Reducing brightness to $DIM_NORMAL%"
	echo "$CUR_BRIGHTNESS" > "$DIM_FILE"
	/usr/bin/xbacklight -set $DIM_NORMAL 
}

strict() {
	[ "$(echo "$CUR_BRIGHTNESS > $DIM_STRICT" | /usr/bin/bc)" -eq 1 ] || exit
	echo "Reducing brightness to $DIM_STRICT%"
	# Only save current brightness if it hasn't previously been lowered
	# by this script (e.g. when waking up from long sleep)
	[ ! -f $DIM_FILE ] && echo "$CUR_BRIGHTNESS" > "$DIM_FILE"
	/usr/bin/xbacklight -set $DIM_STRICT 
}

case $1 in
	"off") off;;
	"normal") normal;;
	"strict") strict;;
	*) usage;;
esac
