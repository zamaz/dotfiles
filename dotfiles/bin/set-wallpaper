#!/bin/bash
WALALPHA=0.5
ALPHA=10

function get_background_brightness() {
	polybar_height=35
	screen_size="$(xrandr -q | grep -Po 'current\s*\K[^,]+' | sed 's/\s*//g' | sort | head -n1)"
	screen_width="${screen_size%x*}"
	screen_height="${screen_size#*x}"

    case $1 in
        bottom )
            img_height_offset="$((screen_height - polybar_height))"
            ;;
        top )
            img_height_offset=0
            ;;
    esac

    croparea="${screen_width}x$polybar_height+0+$img_height_offset"
	convert "$2" \
		-resize "$screen_size^" \
		-crop "$croparea" \
		-colorspace gray \
		-format "%[fx:quantumrange*image.mean]" \
		info: | awk '{printf "%.0f\n", $0/65535*100}'
}

if [ ! -z "$1" ]; then
	# File or directory given
	if [ -f "$1" ] || [ -d "$1" ]; then
		wal -a "$WALALPHA" -i "$1"
	# Reload
	elif [ "$1" == 'reload' ]; then
		wal -a "$WALALPHA" -R
	fi
else
	# No parameter given
	# While this can be done using a parameter switching wallpapers
	# via an i3 keybind has to be aware of the currently active mode.
	if [ -f /tmp/hella ] && [ "$(cat /tmp/hella)" == "1" ]; then
		WALLPAPER_DIR=~/Pictures/life-is-strange
	else
        env="$(cat "$HOME/.cache/env")"
        echo "Environment: $env"
        case $env in
            work )
                WALLPAPER_DIR=~/Pictures/wallpapers
                ;;
            home )
                WALLPAPER_DIR=~/Pictures/wallpapers-home
                ;;
            nsfw )
                WALLPAPER_DIR=~/Pictures/wallpapers-private
                ;;
            hella )
                WALLPAPER_DIR=~/Pictures/life-is-strange
                ;;
            wolfpack )
                WALLPAPER_DIR=~/Pictures/wallpapers-lis2
                ;;
            stalker )
                WALLPAPER_DIR=~/Pictures/wallpapers-stalker
                ;;
            * )
                WALLPAPER_DIR=~/Pictures/wallpapers
                ;;
        esac
	fi
	wal -a "$WALALPHA" -i $WALLPAPER_DIR
fi

source "$HOME/.cache/wal/colors.sh"
NEW_WALLPAPER="$wallpaper" # This comes from sourcing colors.sh
RECENT_WP_FILE="$HOME/.recent_wallpapers"

# Set polybar font color according to background brightness
for loc in top bottom; do
    bg_brightness="$(get_background_brightness "$loc" "$wallpaper")"
    echo "Background brightness behind $loc polybar: $bg_brightness"
    if [[ "$bg_brightness" -gt 50 ]]; then
        echo "polybar.fg_$loc: $background" | xrdb -merge
    else
        echo "polybar.fg_$loc: $foreground" | xrdb -merge
    fi
done

# Restart polybar. Do not launch it if it is not running
if [ -z "$MONITOR" ]; then
	launchpolybar restart&
else
	launchpolybar restart -m "$MONITOR" &
fi

# Create most recent file if it doesn't exist
if [ ! -f "$RECENT_WP_FILE" ]; then
	touch "$RECENT_WP_FILE"
fi

# Save the path of the new wallpaper
echo "New wallpaper: $NEW_WALLPAPER"

# Add the new wallpaper to the top of the list of the most recent ones
# sed fails if file is empty
([ -z "$(cat "$RECENT_WP_FILE")" ] &&
	echo "$NEW_WALLPAPER" >"$RECENT_WP_FILE") || sed -i "1i $NEW_WALLPAPER" "$RECENT_WP_FILE"

# Trim the file containing the most recent wallpapers
most_recent_wps="$(head -5 "$RECENT_WP_FILE")"
echo "$most_recent_wps" >"$RECENT_WP_FILE"

# Inform stupidly-xresources-unaware mlterm about new colors
mlt_main="$HOME/.mlterm/main"
[ ! -f "$mlt_main" ] && mkdir -p "$(dirname "$mlt_main")" && touch "$mlt_main"
if ! grep fg_color "$mlt_main"; then
	echo "fg_color = $foreground" >>"$mlt_main"
else
	sed -i "s/^fg_color\s*=.*$/fg_color = $foreground/g" "$mlt_main"
fi
if ! grep bg_color "$mlt_main"; then
	echo "bg_color = $background$ALPHA" >>"$mlt_main"
else
	sed -i "s/^bg_color\s*=.*$/bg_color = $background$ALPHA/g" "$mlt_main"
fi

# Reload qutebrowser
# qutebrowser grabs focus when reloading. i3-focus-last needs to be started in
# ~/.xsession or ~/.config/i3/config to reassign focus to the previous window
# after reloading
pgrep qutebrowser >/dev/null && qutebrowser :config-source # && \
    #[ -f ~/.i3/focus-last.py ] && ~/.i3/focus-last.py --switch

# Recompile st
cd "$HOME/sourcecode/st" && make clean install
