#!/bin/bash

if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    ID=$ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    ID=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    ID=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    ID=$(cat /etc/debian_version)
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    ID=$(uname -r)
fi

if [ $ID == "arch" ]; then
	sudo pacman -S clang gperf rsync --noconfirm
elif [ $ID == "ubuntu" ]; then
	sudo apt install -y clang gperf texinfo rsync
else
	echo "Unknown distribution $ID. Cannot install clang."
fi

if [[ -z "$(command -v clang)" ]]; then
	echo "clang was not installed correctly. Aborting."
	exit 1
fi

mkdir -p $HOME/sourcecode
cd $HOME/sourcecode

# Install icons in terminal
git clone https://github.com/sebastiencs/icons-in-terminal.git
cd icons-in-terminal
./install.sh
cd ..

# Install ls icons
git clone https://github.com/sebastiencs/ls-icons
cd ls-icons
./bootstrap
export CC=clang CXX=clang++
./configure --prefix=/opt/coreutils
make

if [ $ID == "arch" ]; then
	sudo make install
else
	sudo checkinstall
fi
