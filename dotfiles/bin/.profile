# Make dolphin show icons even when not using KDE
export XDG_CURRENT_DESKTOP=KDE 

# Make xdg-open work
# If this variable is not 4 or 5, xdg-open will try to use kfmclient which
# will result in a Permission denied message. This makes xdg-open use kde-open
# or kde-open5 instead.
export KDE_SESSION_VERSION=5
