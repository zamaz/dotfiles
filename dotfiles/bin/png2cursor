#!/bin/bash
if [ -z "$1" ]; then
	echo "USAGE: png2cursor <cursor name>"
	exit 1
fi
fname="$1"

if [ -z "$2" ]; then
	pos=1
else
	case "$2" in
		"left") pos=0;;
		"center") pos=1;;
		"right") pos=2;;
		*) echo "Invalid position $2 (left, center, right)"; exit 1;;
	esac
fi

if [ -z "$(find . -type f -name "*.png")" ]; then
	echo "Could not find any PNGs here"
	exit 1
fi

# Framerate
fr=75

if [ ! -f xcursorgen.config ]; then
	echo "No xcursorgen config file found"

	# Create xcursorgen config file
	find . -name "*.png" -type f | \
		sort | \
		while read -r line; do echo "16 8 8 $line $fr"; done \
			> xcursorgen.config || (echo "Failed to create one" && exit 1)
	echo "I created one for you but you should check if the files are sorted correctly and if you like the delay value"
	cat xcursorgen.config
	exit 1
fi

resolutions=(16 32 64 128)
for res in "${resolutions[@]}"; do
	case "$pos" in
		0) hres=0;;
		1) hres=$(( res / 2 ));;
		2) hres="$res";;
	esac
	folder="$res"x"$res"
	mkdir -p "$folder"

	# Convert each PNG to this resolution and save to the appropriate folder
	for f in *.png; do
		convert "$f" -resize "$folder" "$folder/$(basename "$f")"
	done

	# Copy xcursorgen config and replace resolution
	awk -v res="$res" -v hres="$hres" \
		'{print res " " hres " " hres " " $4 " " $5}' xcursorgen.config \
		> "$folder/xcursorgen.config" || (echo "Failed to copy xcursoregen config" && exit 1)

	# Create the cursor file
	xcursorgen -p "$folder" "$folder/xcursorgen.config" "$folder/$fname.cursor" || \
		(echo "Failed to create cursor file" && exit 1)
	echo "Created $folder $fname cursor file"
done

echo "SUCCESS!"
