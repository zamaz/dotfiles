#!/bin/bash
# Arguments: $1 - LastPass folder name

# Get currently active terminal pid
CURRENT_SHELL="$(echo "$SHELL" | xargs basename)"
CURRENT_WIN_ID="$(xprop -root _NET_ACTIVE_WINDOW | awk '{print $NF}')"
CURRENT_WIN_PID="$(xprop -id "$CURRENT_WIN_ID" _NET_WM_PID | awk '{print $NF}')"
# Find the shell that has the active terminal as parent
CURRENT_PTS="$(ps axo pid,ppid,tty,comm | awk -v "pid=$CURRENT_WIN_PID" -v "shell=$CURRENT_SHELL" '($2==pid && $4==shell) { print }' | awk '{print $3}')"

if ! lpass status -q; then
	#account="$(echo 'smirni.sersala@gmail.com' | rofi -dmenu -p "lpass account")"
	account='smirni.sersala@gmail.com'
	lpass login "$account"
fi
lpass status -q || notify-send "rofi pass" "You have to log in to lpass first" || exit 1

# If argument was given only show accounts in that folder
if [[ -z "$1" ]]; then
	# Remove non-existing groups; highlight groups
    account="$(lpass ls | sed 's/^(none)\///g' | sed 's/^\(.*\)\(\[id: [0-9]*\]\)/<b>\1<\/b> <span weight="light">\2<\/span>/g' |
		rofi -dmenu -i -markup-rows -p "LastPass - Get password for" -matching fuzzy)"
else
	# Remove group
	account="$(lpass ls "$1" | sed 's/^.*\///g' | sed 's/^\(.*\)\(\[id: [0-9]*\]\)/<b>\1<\/b> <span weight="light">\2<\/span>/g' |
		rofi -dmenu -i -markup-rows -p "LastPass - Get password for" -matching fuzzy)"
fi

# Extract account ID
account="$( echo "$account" | sed 's/^.*\[id: \([0-9]*\)\].*$/\1/g')"
[ -z "$account" ] && exit 0

pass="$(lpass show $account --password)"
[ -z "$pass" ] && pass="$(lpass show $account --notes)"
[ -z "$pass" ] && notify-send "rofi pass" "Failed to get password for $account" && exit 1

# Write password directly to terminal if one has focus. Else, copy to clipboard and paste it.
if [ -z "$CURRENT_PTS" ]; then
	echo -n "$pass" | xclip -i -selection clipboard # && xdotool key Ctrl+V
	notify-send "rofi pass" "Password for $account copied to clipboard"
else
	PTS_PATH="/dev/$CURRENT_PTS"
	ttyecho -n "$PTS_PATH" "$pass"
fi
