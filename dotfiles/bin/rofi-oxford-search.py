from oxford import Word, WordNotFound
import subprocess
import sys
import textwrap

def wrap_line(line):
    return '\n  '.join(textwrap.wrap(line, 89))

if __name__ == '__main__':
    if len(sys.argv) > 1:
        term = sys.argv[1]
    else:
        exit()

    pronounce = False
    if len(sys.argv) > 2 and sys.argv[2] in ('p', 'pronounce'):
        pronounce = True

    try:
        Word.get(term)
    except WordNotFound:
        print('No result')
        exit()

    heading = "<b>{} ({})</b>".format(Word.name(), Word.wordform())
    print(heading)

    if len(Word.definitions()):
        print()
        print('<b><u>Definitions</u></b>')
        for defi in Word.definitions():
            print(wrap_line('‣ ' + defi))

    if len(Word.examples()):
        print()
        print('<b><u>Examples</u></b>')
        for ex in Word.examples():
            print('"{}"'.format(ex))

    if len(Word.idioms()):
        print()
        print('<b><u>Idioms</u></b>')
        for ex in Word.idioms():
            print("<b>{}</b>".format(ex['name']))
            for defi in ex['definitions']:
                print('  - ' + defi['description'])
                for x in defi['examples']:
                    print('    <i>"{}"</i>'.format(x))

    if len(Word.phrasal_verbs()):
        print()
        print('<b><u>Phrasal verbs</u></b>')
        for ex in Word.phrasal_verbs():
            print(ex['name'])

    if len(Word.synonyms()):
        print()
        print('<b><u>Synonyms</u></b>')
        for syn in Word.synonyms():
            if syn['label']:
                name = "{} ({})".format(syn['name'], syn['label'])
            else:
                name = "{}".format(syn['name'])

            if syn['search']:
                name += '  |  🔍 ' + syn['search']

            # Only print the synonym in bold if there is a definition following
            if syn['definition']:
                print("<b>{}</b>".format(name))
                print("  {}".format(syn['definition']))
            else:
                print("{}".format(name))

            for ex in syn['examples']:
                print("  - {}".format(ex))

    if Word.origins():
        print()
        print('<b><u>Origin</u></b>')
        for origin in Word.origins():
            print(wrap_line('‣ ' + origin))

    if Word.other_results() and len(Word.other_results()):
        print()
        print('<b><u>Other results</u></b>')
        results = Word.other_results()[0]
        for ex in results['All matches']:
            try:
                wordform = ex['wordform']
            except KeyError:
                wordform = 'phrase'
            res = "<b>{}</b> <i>({})</i>".format(ex['name'], wordform)
            res += "  |  🔍 " + "{}".format(ex['id'])
            print(res)

    if Word.pronunciations():
        print()
        print('<b><u>Pronunciation</u></b>')
        gb, us = Word.pronunciations()
        print('GB: {}'.format(gb['ipa']))
        print('US: {}'.format(us['ipa']))
        if pronounce:
            cmd = "mplayer " + us['url']
            process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            process.communicate()
