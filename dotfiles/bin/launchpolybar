#!/usr/bin/env sh

usage() { echo "Usage: $0 [-s <SCREEN>] [-m <MODE=duplicate|primary>]" 1>&2; exit 1; }

# Terminate already running bar instances
TOP_BAR=0
BOTTOM_BAR=0
ps ax -o pid,args | grep "polybar top" | grep -qv grep && TOP_BAR=1
ps ax -o pid,args | grep "polybar bottom" | grep -qv grep && BOTTOM_BAR=1

[ "$1" == "restart" ] && [ $TOP_BAR -ne 1 ] && [ $BOTTOM_BAR -ne 1 ] && exit
killall polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do killall polybar; done

# Get arguments
while getopts "m:s:" option; do
	case "$option" in
		m) MODE="$OPTARG";;
		s) MONITOR="$OPTARG";;
		*) usage;;
	esac
done

echo "Launching polybar..."

# Show on single monitor
if [ ! -z $MONITOR ]; then
	MONITOR=$MONITOR polybar --reload bottom 2>/dev/null &
	#MONITOR=$MONITOR polybar --reload top 2>/dev/null &
	exit 0
fi
[[ $TOP_BAR -eq 1 ]] && echo "Top bar running"
# Show on all screens
if [ "$MODE" = 'duplicate' ]; then
	for m in $(polybar --list-monitors | cut -d":" -f1); do
		MONITOR=$m polybar --reload bottom 2>/dev/null &
        [[ "$TOP_BAR" -eq 1 ]] && MONITOR=$m polybar --reload top 2>/dev/null &
		#MONITOR=$m polybar --reload top 2>/dev/null &
	done
	exit 0
# Show on primary screen
elif [ -z $MODE ] || [ "$MODE" = 'primary' ]; then
	PRIMARY="$(xrandr | grep -E "\bconnected\b primary" | cut -d' ' -f1)"
	if [ -z $PRIMARY ]; then
		notify-send "polybar" "No primary monitor configured<br>Using first in randr output"
		PRIMARY="$(xrandr | grep -E "\bconnected\b" | head -1 | cut -d' ' -f1)"
	fi
    echo "Launching bottom bar"
	MONITOR=$PRIMARY polybar bottom 2>/dev/null &
    [[ "$TOP_BAR" -eq 1 ]] && echo "Launching top bar" && MONITOR=$PRIMARY polybar top 2>/dev/null &
	#MONITOR=$PRIMARY polybar top 2>/dev/null || polybar top
	exit 0
else
	echo "Unkown mode $MODE"
fi
