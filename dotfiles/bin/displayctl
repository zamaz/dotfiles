#!/bin/bash
#
# Configure monitor setups with xrandr. This script sould be added as a
# udev rule that gets triggered when a monitor cable is plugged in
# Watch what happens when you plug in a monitor with `udevadm monitor`
# Create the file /etc/udev/rules.d/99-monitor-hotplug.rules with the
# content 
# "ACTION=="change", SUBSYSTEM=="drm", HOTPLUG=="1", RUN+="path/to/this/script""
# https://ruedigergad.com/2012/01/28/hotplug-an-external-screen-to-your-laptop-on-linux/
# For troubleshooting, see
# https://unix.stackexchange.com/questions/14854/xrandr-command-not-executed-within-shell-command-called-from-udev-rule
#export HOME=/home/zama
#export DISPLAY=:0
#export XAUTHORITY=$HOME/.Xauthority
#export PATH="$HOME/bin:$PATH"

SLEEP=0.5
DISPLAYS="$(xrandr -d :0 --query | grep '\bconnected\b' | cut -d' ' -f1)"

FIRST_DISPLAY=eDP-1
HOME_DISPLAY_1=HDMI2
WORK_DISPLAY_1=DP-2-1
WORK_DISPLAY_2=DP-2-2

SECOND_DISPLAY=
THIRD_DISPLAY=

echo "Connected displays: $DISPLAYS"
echo "Environments:
 - Home:              $HOME_DISPLAY_1
 - Work (2 monitors):  $WORK_DISPLAY_1
 - Work (3 monitors): $WORK_DISPLAY_2

"
xrandr -d :0 \
	--output "$HOME_DISPLAY_1" --off \
	--output "$WORK_DISPLAY_1" --off \
	--output "$WORK_DISPLAY_2" --off \
	--output "$FIRST_DISPLAY" --off
xrandr -d :0 --auto

case "$1" in
	single)
		/home/zama/.screenlayout/single.sh
		exit
		;;
	projector)
		/home/zama/.screenlayout/projector.sh
		exit
		;;
	dual)
		/home/zama/.screenlayout/dual-screen.sh
		exit
		;;
	dual-ext)
		/home/zama/.screenlayout/dual-ext.sh
		exit
		;;
esac

if [[ "$DISPLAYS" =~ $WORK_DISPLAY_1 ]] && [[ ! "$DISPLAYS" =~ $WORK_DISPLAY_2 ]]; then
	echo "Work environment with 2 monitors detected"
	/home/zama/.screenlayout/dual-screen.sh
	MONITOR=$WORK_DISPLAY_1
	# Size of one lo-res monitor: 1.5*(1920x1200) = (2880x1800)
	# Total width: 2*2880 = 5760
	# Total height: 1800 + 1440 = 3240
	#SECOND_DISPLAY="$WORK_DISPLAY_1"
	#res2=1920x1200
	#fbres2=5760x3240
	#pos1=320x1800
	#scale=1.5x1.5
elif [[ "$DISPLAYS" =~ $HOME_DISPLAY_1 ]]; then
	echo "Home environment detected"
	MONITOR=$HOME_DISPLAY_1
	# Size of one lo-res monitor: 1.5*(1920x1080) = (2880x1620)
	# Total width: 2*2880 = 5760
	# Total height: 1620 + 1440 = 3060
	#SECOND_DISPLAY="$HOME_DISPLAY_1"
	#res2=1920x1080
	#fbres2=5760x3060
	#pos1=320x1620
	#scale=1.5x1.5
elif [[ "$DISPLAYS" =~ $WORK_DISPLAY_2 ]]; then
	echo "Work environment with 3 monitors detected"
	/home/zama/.screenlayout/triple-screen.sh
	MONITOR=$WORK_DISPLAY_1
	#THIRD_DISPLAY="$WORK_DISPLAY_2"
	#res3=1920x1200
	#fbres3=5760x3240
else
	/home/zama/.screenlayout/single.sh
fi

echo "Primary monitor: $MONITOR"
export MONITOR

sleep $SLEEP
~/.i3/merge-workspaces.py
set-wallpaper reload
launchconky
