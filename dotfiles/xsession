#!/bin/sh
# This file gets sourced by /usr/share/sddm/scripts/Xsession when logging in
# with sddm. Exporting PATH here lets apllications such as i3 and rofi know
# about it.
export PATH=$HOME/.gem/ruby/2.6.0/bin:$HOME/.local/bin:$HOME/mpcdf/bin:$HOME/bin:$HOME/bin/i3-workspaces:/usr/local/bin:$PATH

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk

# Start keybinding daemon
sxhkd&

# Configure touchpad
xinput set-prop 'Elan Touchpad' 'libinput Tapping Enabled' 1 
xinput set-prop 'Elan Touchpad' 'libinput Tapping Enabled Default' 1 
xinput set-prop 'Elan Touchpad' 'libinput Tapping Drag Enabled' 1 
xinput set-prop 'Elan Touchpad' 'libinput Tapping Drag Enabled Default' 1 
xinput set-prop 'Elan Touchpad' 'libinput Disable While Typing Enabled' 1 
xinput set-prop 'Elan Touchpad' 'libinput Disable While Typing Enabled Default' 1 
# Disable red Lenovo mouse button (TrackPoint)
# https://unix.stackexchange.com/questions/32992/thinkpad-disable-trackpoint
xinput set-prop "ETPS/2 Elantech TrackPoint" "Device Enabled" 0

# Workaround for signal to work
# https://github.com/signalapp/Signal-Desktop/issues/2707
# But: setting TMPDIR globally will make other applications such as mutt use
# this folder too. Set it right before starting signal instead using zshrc and
# the desktop file
mkdir -p "$HOME/.cache/signal"
#export TMPDIR="$HOME/.cache/signal"

# Have lastpass ask for the master password every six hours
export LPASS_AGENT_TIMEOUT=21600

# Use Qt pinentry window for lastpass by default
# If used in a tty pinentry automatically falls back to the curses version
export LPASS_PINENTRY=/usr/bin/pinentry-qt

export SSH_ENV="$HOME/.ssh/env"

function export_ssh_vars(){
    [ -f "$SSH_ENV" ] && \
        . "${SSH_ENV}" > /dev/null && \
        export $(cut -d= -f1 "$SSH_ENV")
}

ssh-agent | sed '/^echo/d' > "${SSH_ENV}"
chmod 600 "${SSH_ENV}"
export_ssh_vars

# Remap PgUp and PgDown since I always accidentally hit them when trying to press Left or Right
xmodmap -e 'keycode 112 = Left'
xmodmap -e 'keycode 117 = Right'

# Map CapsLock to ESC for easier vimming and because nobody needs CapsLock
# https://unix.stackexchange.com/questions/199266/how-to-permanently-remap-caps-lock-to-esc-in-x11
xmodmap -e "clear lock" # Without this, CapsLock will still lock caps
xmodmap -e "keycode 66 = Escape NoSymbol Escape"

# Monitor set up is done by SDDM before showing the login screen via
# /usr/share/sddm/scripts/Xsetup
set-wallpaper
launchconky
launchcompton

# Set GTK3 theme
GTK_THEME=Adapta-Nokto-Eta:dark
QT_QPA_PLATFORMTHEME=qt5ct

## Hide mouse cursor after a few seconds
unclutter --timeout 5 --jitter 10 --ignore-scrolling&

## Prevent screen lock and sleep if application is in fullcreen mode
caffeine&

## Show FM4 module on polybar
echo 'Play Radio FM4' > /tmp/mplayer-now-playing

## Auto-lock screen after some time of inactivity
xautolock -time 5 -notify 30 -notifier notify-send.sh -secure -locker "i3lock-fancy-rapid 10 5" -detectsleep&

## Start notification deamon
/usr/lib/x86_64-linux-gnu/notify-osd&

## Start notification center
dead-notification-center&

## Delete dmenu cache. This ensures up-to-date menu entries
rm -f ~/.dmenu_cache
rm -f ~/.cache/dmenu_run

# Start music daemon
mopidy&
