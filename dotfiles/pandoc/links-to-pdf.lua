function Link(el)
	isURL = string.match(el.target, '^https?://') or string.match(el.target, '^www[0-9]*.')
	if not isURL then
  	el.target = el.target .. ".pdf"
	end
  return el
end
