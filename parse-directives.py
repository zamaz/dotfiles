#!/usr/bin/python3
import socket
import sys
import os

DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "dotdrop")
sys.path.append(DIR)
from dotdrop.templategen import Templategen

args = sys.argv

if len(args) not in (2, 3):
    print("Usage: {} <infile> <outfile>".format(args[0]))
    exit(1)

infile = args[1]

if len(args) < 3:
    outfile = os.path.join('/tmp', os.path.basename(infile))
else:
    outfile = args[2]

if os.path.isfile(infile):
    infile = os.path.abspath(infile)
else:
    print("infile: {} is not a valid path".format(infile))
    exit(1)
if os.path.isdir(os.path.dirname(outfile)):
    outfile = os.path.abspath(outfile)
else:
    print("outfile: {} is not a valid path".format(outfile))
    exit(1)

t = Templategen()
content = t.generate(infile).decode('utf-8')

try:
    with open(outfile, 'w') as f:
        f.write(content)
except Exception as e:
    print(str(e))
    exit(1)
else:
    print("Created temporary parsed file " + outfile)
    exit(0)
