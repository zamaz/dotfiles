#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ ! -z "$1" ] && [ ! -d "$1" ]; then
	echo "Target $1 is not a valid directory path"
	exit 1
elif [ -d "$1" ]; then
	TARGET="$1"
else
	TARGET="$HOME/.local/share/applications/"
fi

echo -e "Installing desktop files here: $TARGET\n"

for file in $DIR/*.desktop; do
	name="$(awk -F'=' '/^Name=/ {print $2}' "$file" | head -n1)"
	awk -F'[=]' '/^Exec=/ {split($NF,a," "); if (NF-1 > 1){print a[2]} else {print a[1]}}' "$file" | \
        sed 's/"//g' | \
        while read -r bin; do
		    installed=1
		    if ! command -v "$bin" >/dev/null; then
		    	echo -e "\033[31mSkipping $name: $bin not installed.\033[0m"
		    	installed=0
		    	break
		    fi
		    [ $installed -eq 0 ] && continue
            if desktop-file-validate "$file"; then
		        desktop-file-install --dir="$TARGET" "$file" && \
                echo -e "\033[32mInstalled $name\033[0m"
            else
                echo "Desktop file $name invalid"
            fi
            #sudo rm "$TARGET/$(basename "$file")" && \
            #    echo -e "\033[32mRemoved $name\033[0m"
	    done
done
