sudo apt install compton \
 blueman \
 w3m \
 rxvt-unicode \
 feh \
 nmap \
 tlp \
 libasound2-dev \
 libnl-genl-3-dev \
 libmpdclient-dev \
 mopidy \
 mplayer \
 gawk \
 libcairo2-dev \
 conky \
 libtolua-dev libtolua++5.1-dev libimlib2-dev libncurses5-dev libx11-dev libxdamage-dev libxft-dev libxinerama-dev libxml2-dev \
 ccmake \
 libcurl4 \
 libcurl4-openssl-dev \
 cmake-curses-gui \
 libiw-dev \
 lua5.3 \
 conky \
 lua5.1 \
 lua5.1-dev \
 librsvg-2.0 \
 librsvg2-bin \
 librsvg2-dev \
 xbacklight \
 xcb-util-xrm \
 libxcb-xrm-dev \
 thunderbird \
 php7.2 mysql-client apache2 \
 redshift \
 libinput \
 xserver-xorg-input-libinput \
 shutter \
 rofi \
 terminology \
 scrot \
 ffmpeg \
 indicator-stickynotes \
 xautolock \
 lastpass-cli \
 texlive-latex-recommended \
 mupdf \
 hunspell-de-de \
 texlive-lang-german \
 xzdec \
 libnotify-bin notify-osd \
 dunst \
 w3m \
 w3m-img \
 silversearcher-ag \
 ack \
 ack-grep \
 todoist \
 golang \
 xclip \
 gparted \
 ktorrent

git clone https://github.com/hastinbe/i3-volume
git clone --branch 3.2 --recursive https://github.com/jaagr/polybar
git clone git@gitlab.lrz.de:zamaz/bin-scripts.git bin
git clone https://github.com/brndnmtthws/conky
git clone https://gitlab.mpcdf.mpg.de/zama/thundersync
git clone https://github.com/jcs/xbanish
git clone https://github.com/UtkarshVerma/installer-scripts
git clone https://github.com/meskarune/i3lock-fancy
git clone https://github.com/DaveDavenport/rofi
git clone https://github.com/sachaos/todoist.git
