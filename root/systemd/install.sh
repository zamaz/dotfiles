#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

sudo mkdir -p /usr/local/lib/systemd/system/
for file in "$(find "$DIR" -type f ! -name install.sh)"; do
    sudo cp -vf "$file" /usr/local/lib/systemd/system/
done
