#!/bin/sh
ESSID="$(iwgetid -r)"

logger "Connected to: $ESSID"
logger "Status: $2"

interface=$1 status=$2
case $status in
  up|vpn-down)
    if iwgetid | grep -qs ":\"$ESSID\""; then
	    logger "Activating vpn"
      /home/zama/mpcdf/bin/vpn
    fi
    ;;
  down)
    if iwgetid | grep -qs ":\"$ESSID\""; then
      if nmcli connection show --active | grep "$VPN_NAME"; then
	    logger "Deactivating vpn"
      	/home/zama/mpcdf/bin/vpn off
      fi
    fi
    ;;
esac
