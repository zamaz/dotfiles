#!/bin/bash

cd ~/dotfiles

# Check if files are synced
CHANGED_FILES="$(./dotdrop.sh compare -b 2>/dev/null | grep diffing | grep -v f_konsolerc | grep -v f_main | cut -d\" -f4)"

# Check if files are synced but not committed
UNSTAGED="$(git diff-files --ignore-submodules)"
UNCOMMITTED="$(git diff-index --cached --ignore-submodules HEAD --)"

# Check against online repo
# See https://github.com/bhilburn/powerlevel9k/blob/master/functions/vcs.zsh
BRANCH=$(git symbolic-ref --short HEAD 2>/dev/null)
AHEAD=$(git rev-list "${BRANCH}"@{upstream}..HEAD 2>/dev/null | wc -l)
BEHIND=$(git rev-list HEAD.."${BRANCH}"@{upstream} 2>/dev/null | wc -l)

if [ ! -z "$CHANGED_FILES" ]; then
	echo "Dotfiles outdated"
elif [ ! -z "$UNSTAGED" ]; then
	echo "Dotfiles: Unstaged changes"
elif [ ! -z "$UNCOMMITTED" ]; then
	echo "Dotfiles: Uncommitted changes"
elif [ ! "$BEHIND" = "0" ]; then
	echo "Dotfiles: Unpulled changes"
elif [ ! "$AHEAD" = "0" ]; then
	echo "Dotfiles: Unpushed changes"
else
	echo ""
fi

if [ "$1" = "resolve" ]; then
	if [ ! -z "$CHANGED_FILES" ]; then
		./diff.sh
	elif [ ! -z "$UNSTAGED" ]; then
		git status
	elif [ ! -z "$UNCOMMITTED" ]; then
		git status
	elif [ ! "$BEHIND" = "0" ]; then
		git pull
	elif [ ! "$AHEAD" = "0" ]; then
		git push
	fi
fi
